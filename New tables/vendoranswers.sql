/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : xorbdcom_admission_ninja

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2018-05-22 12:19:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `vendoranswers`
-- ----------------------------
DROP TABLE IF EXISTS `vendoranswers`;
CREATE TABLE `vendoranswers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendorquestion_id` int(11) DEFAULT NULL,
  `answer` varchar(255) COLLATE utf32_unicode_ci DEFAULT NULL,
  `is_correct` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- ----------------------------
-- Records of vendoranswers
-- ----------------------------
INSERT INTO `vendoranswers` VALUES ('1', '1', 'test 1\r\n', null, '2018-05-12 18:05:53', '2018-05-12 18:05:53');
INSERT INTO `vendoranswers` VALUES ('2', '1', 'test 2\r\n', null, '2018-05-12 18:05:53', '2018-05-12 18:05:53');
INSERT INTO `vendoranswers` VALUES ('3', '1', 'test 3\r\n', null, '2018-05-12 18:05:53', '2018-05-12 18:05:53');
INSERT INTO `vendoranswers` VALUES ('4', '1', 'test 4\r\n', '1', '2018-05-12 18:05:53', '2018-05-12 18:05:53');
INSERT INTO `vendoranswers` VALUES ('5', '4', '1', null, '2018-05-13 07:48:33', '2018-05-13 07:48:33');
INSERT INTO `vendoranswers` VALUES ('6', '4', '2', null, '2018-05-13 07:48:33', '2018-05-13 07:48:33');
INSERT INTO `vendoranswers` VALUES ('7', '4', '3', null, '2018-05-13 07:48:33', '2018-05-13 07:48:33');
INSERT INTO `vendoranswers` VALUES ('8', '4', '4', '1', '2018-05-13 07:48:33', '2018-05-13 07:48:33');

-- ----------------------------
-- Table structure for `vendorquestions`
-- ----------------------------
DROP TABLE IF EXISTS `vendorquestions`;
CREATE TABLE `vendorquestions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `question` varchar(255) COLLATE utf32_unicode_ci DEFAULT NULL,
  `answer_description` varchar(255) COLLATE utf32_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- ----------------------------
-- Records of vendorquestions
-- ----------------------------
INSERT INTO `vendorquestions` VALUES ('1', '88', null, 'test\r\n', 'fgd\r\n', '2018-05-12 18:05:53', '2018-05-12 18:05:53');
INSERT INTO `vendorquestions` VALUES ('2', '88', null, 'Test\r\n', 'tsdsaj shdjsa hsdb a\r\n', '2018-05-13 05:39:58', '2018-05-13 05:39:58');
INSERT INTO `vendorquestions` VALUES ('3', '88', null, 'Test', 'tsdsaj shdjsa hsdb a', '2018-05-13 05:43:02', '2018-05-13 05:43:02');
INSERT INTO `vendorquestions` VALUES ('4', '88', null, 'super', 'dgrthrt', '2018-05-13 07:48:33', '2018-05-13 07:48:33');

-- ----------------------------
-- Table structure for `vendorresults`
-- ----------------------------
DROP TABLE IF EXISTS `vendorresults`;
CREATE TABLE `vendorresults` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendortest_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `mark` float DEFAULT NULL,
  `exam_time` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- ----------------------------
-- Records of vendorresults
-- ----------------------------
INSERT INTO `vendorresults` VALUES ('1', '4', '88', '60', null, '2018-05-13 21:03:25', '2018-05-13 21:03:35');

-- ----------------------------
-- Table structure for `vendortests`
-- ----------------------------
DROP TABLE IF EXISTS `vendortests`;
CREATE TABLE `vendortests` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(10) DEFAULT NULL,
  `title` varchar(255) COLLATE utf32_unicode_ci DEFAULT NULL,
  `instructions` text COLLATE utf32_unicode_ci,
  `test_time` int(10) DEFAULT NULL,
  `status` varchar(255) COLLATE utf32_unicode_ci DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- ----------------------------
-- Records of vendortests
-- ----------------------------
INSERT INTO `vendortests` VALUES ('1', null, '20', 'abc', '', '600', 'Running ', '2018-05-12 23:19:00', '2018-05-12 19:19:34', '2018-05-12 19:19:34');
INSERT INTO `vendortests` VALUES ('2', null, '21', 'bcd', '', '2400', 'Done', '2018-05-30 23:24:00', '2018-05-12 19:24:20', '2018-05-12 19:24:20');
INSERT INTO `vendortests` VALUES ('3', null, '19', 'dxcfghjkl;', '', '600', 'Expired', '2018-05-12 23:30:00', '2018-05-12 19:30:21', '2018-05-12 19:30:21');
INSERT INTO `vendortests` VALUES ('4', '88', '19', 'mukul', '', '600', 'Done', '2018-05-13 08:58:00', '2018-05-13 05:00:22', '2018-05-13 05:00:22');

-- ----------------------------
-- Table structure for `vendortests_vendorquestions`
-- ----------------------------
DROP TABLE IF EXISTS `vendortests_vendorquestions`;
CREATE TABLE `vendortests_vendorquestions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `vendortest_id` int(10) DEFAULT NULL,
  `vendorquestion_id` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- ----------------------------
-- Records of vendortests_vendorquestions
-- ----------------------------
