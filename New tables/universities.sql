/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : xorbdcom_admission_ninja

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2018-05-18 10:54:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `universities`
-- ----------------------------
DROP TABLE IF EXISTS `universities`;
CREATE TABLE `universities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(555) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of universities
-- ----------------------------
INSERT INTO `universities` VALUES ('1', 'Sylhet Agricultural University');
INSERT INTO `universities` VALUES ('2', 'Bangabandhu Sheikh Mujibur Rahman Agricultural University');
INSERT INTO `universities` VALUES ('3', 'Sylhet Agricultural University');
INSERT INTO `universities` VALUES ('4', 'Sher-e-Bangla Agricultural University');
INSERT INTO `universities` VALUES ('5', 'Bangladesh Agricultural University');
INSERT INTO `universities` VALUES ('6', 'Bangabandhu Sheikh Mujibur Rahman Science & Technology University');
INSERT INTO `universities` VALUES ('7', 'Hajee Mohammad Danesh Science & Technology University');
INSERT INTO `universities` VALUES ('8', 'Jessore University of Science & Technology');
INSERT INTO `universities` VALUES ('9', 'Mawlana Bhashani Science & Technology University');
INSERT INTO `universities` VALUES ('10', 'Noakhali Science & Technology University');
INSERT INTO `universities` VALUES ('11', 'Pabna University of Science and Technology');
INSERT INTO `universities` VALUES ('12', 'Patuakhali Science And Technology University');
INSERT INTO `universities` VALUES ('13', 'Rangamati Science and Technology University');
INSERT INTO `universities` VALUES ('14', 'Shahjalal University of Science & Technology');
INSERT INTO `universities` VALUES ('15', 'Bangladesh University of Engineering & Technology');
INSERT INTO `universities` VALUES ('16', 'Chittagong University of Engineering & Technology');
INSERT INTO `universities` VALUES ('17', 'Dhaka University of Engineering & Technology');
INSERT INTO `universities` VALUES ('18', 'Khulna University of Engineering and Technology');
INSERT INTO `universities` VALUES ('19', 'Rajshahi University of Engineering & Technology');
INSERT INTO `universities` VALUES ('20', 'Bangladesh Open University');
INSERT INTO `universities` VALUES ('21', 'Bangladesh University of Professionals');
INSERT INTO `universities` VALUES ('22', 'Barisal University');
INSERT INTO `universities` VALUES ('23', 'Begum Rokeya University');
INSERT INTO `universities` VALUES ('24', 'Comilla University');
INSERT INTO `universities` VALUES ('25', 'Islamic Arabic University');
INSERT INTO `universities` VALUES ('26', 'Islamic University');
INSERT INTO `universities` VALUES ('27', 'Jagannath University');
INSERT INTO `universities` VALUES ('28', 'Jahangirnagar University');
INSERT INTO `universities` VALUES ('29', 'Jatiya Kabi Kazi Nazrul Islam University');
INSERT INTO `universities` VALUES ('30', 'Khulna University');
INSERT INTO `universities` VALUES ('31', 'National University');
INSERT INTO `universities` VALUES ('32', 'University of Chittagong');
INSERT INTO `universities` VALUES ('33', 'University of Dhaka');
INSERT INTO `universities` VALUES ('34', 'University of Rajshahi');
INSERT INTO `universities` VALUES ('35', 'Bangladesh University of Textiles');
INSERT INTO `universities` VALUES ('36', 'Chittagong Medical University');
INSERT INTO `universities` VALUES ('37', 'Chittagong Veterinary and Animal Sciences University');
INSERT INTO `universities` VALUES ('38', 'Rabindra University, Bangladesh');
INSERT INTO `universities` VALUES ('39', 'Rajshahi Medical University');
INSERT INTO `universities` VALUES ('40', 'College of Textile Technology');
