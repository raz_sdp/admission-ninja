/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : an_test

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2018-05-28 14:32:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `vendorusers`
-- ----------------------------
DROP TABLE IF EXISTS `vendorusers`;
CREATE TABLE `vendorusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vendorusers
-- ----------------------------
INSERT INTO `vendorusers` VALUES ('1', '126', '56', '2018-05-24 07:59:44', '2018-05-24 07:59:44');
INSERT INTO `vendorusers` VALUES ('2', '126', '56', '2018-05-24 08:02:33', '2018-05-24 08:02:33');
INSERT INTO `vendorusers` VALUES ('3', '126', null, '2018-05-24 10:02:13', '2018-05-24 10:02:13');
INSERT INTO `vendorusers` VALUES ('4', '126', null, '2018-05-24 10:05:13', '2018-05-24 10:05:13');
INSERT INTO `vendorusers` VALUES ('5', '126', '91', '2018-05-24 10:44:27', '2018-05-24 10:44:27');
INSERT INTO `vendorusers` VALUES ('6', '126', '97', '2018-05-24 10:49:51', '2018-05-24 10:49:51');
INSERT INTO `vendorusers` VALUES ('7', '126', '115', '2018-05-24 10:53:08', '2018-05-24 10:53:08');
INSERT INTO `vendorusers` VALUES ('8', '126', '83', '2018-05-24 10:57:15', '2018-05-24 10:57:15');
INSERT INTO `vendorusers` VALUES ('9', '126', '110', '2018-05-24 10:58:56', '2018-05-24 10:58:56');
INSERT INTO `vendorusers` VALUES ('10', '126', '99', '2018-05-24 11:02:07', '2018-05-24 11:02:07');
INSERT INTO `vendorusers` VALUES ('11', '126', '69', '2018-05-24 11:02:58', '2018-05-24 11:02:58');
INSERT INTO `vendorusers` VALUES ('14', '118', '104', '2018-05-24 18:08:24', '2018-05-24 18:08:24');
INSERT INTO `vendorusers` VALUES ('15', '118', '86', '2018-05-24 18:09:02', '2018-05-24 18:09:02');
INSERT INTO `vendorusers` VALUES ('16', '118', '100', '2018-05-24 18:11:14', '2018-05-24 18:11:14');
INSERT INTO `vendorusers` VALUES ('17', '118', '97', '2018-05-24 18:15:33', '2018-05-24 18:15:33');
INSERT INTO `vendorusers` VALUES ('18', '118', '110', '2018-05-25 04:55:36', '2018-05-25 04:55:36');
INSERT INTO `vendorusers` VALUES ('19', '118', '91', '2018-05-25 05:00:01', '2018-05-25 05:00:01');
INSERT INTO `vendorusers` VALUES ('20', '118', '91', '2018-05-25 05:01:13', '2018-05-25 05:01:13');
INSERT INTO `vendorusers` VALUES ('21', '118', '91', '2018-05-25 05:02:10', '2018-05-25 05:02:10');
INSERT INTO `vendorusers` VALUES ('22', '118', '91', '2018-05-25 05:02:26', '2018-05-25 05:02:26');
INSERT INTO `vendorusers` VALUES ('23', '118', '91', '2018-05-25 05:03:20', '2018-05-25 05:03:20');
INSERT INTO `vendorusers` VALUES ('24', '118', '99', '2018-05-25 05:04:45', '2018-05-25 05:04:45');
INSERT INTO `vendorusers` VALUES ('25', '118', '117', '2018-05-26 07:28:57', '2018-05-26 07:28:57');
INSERT INTO `vendorusers` VALUES ('26', '126', '91', '2018-05-26 07:46:00', '2018-05-26 07:46:00');
INSERT INTO `vendorusers` VALUES ('27', '126', '69', '2018-05-26 07:50:12', '2018-05-26 07:50:12');
INSERT INTO `vendorusers` VALUES ('28', '126', '83', '2018-05-26 07:52:57', '2018-05-26 07:52:57');
INSERT INTO `vendorusers` VALUES ('29', '126', '93', '2018-05-26 07:55:14', '2018-05-26 07:55:14');
INSERT INTO `vendorusers` VALUES ('30', '59', '93', '2018-05-26 08:10:03', '2018-05-26 08:10:03');
INSERT INTO `vendorusers` VALUES ('31', '118', '120', '2018-05-26 08:15:33', '2018-05-26 08:15:33');
INSERT INTO `vendorusers` VALUES ('32', '118', '92', '2018-05-28 05:14:32', '2018-05-28 05:14:32');
INSERT INTO `vendorusers` VALUES ('33', '118', '91', '2018-05-28 05:16:30', '2018-05-28 05:16:30');
INSERT INTO `vendorusers` VALUES ('34', '118', '91', '2018-05-28 05:17:56', '2018-05-28 05:17:56');
INSERT INTO `vendorusers` VALUES ('35', '118', '91', '2018-05-28 05:18:04', '2018-05-28 05:18:04');
