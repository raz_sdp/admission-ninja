<?php
App::uses('AppModel', 'Model');
/**
 * UsersLecture Model
 *
 * @property User $User
 * @property Lecture $Lecture
 */
class UsersLecture extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Lecture' => array(
			'className' => 'Lecture',
			'foreignKey' => 'lecture_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
