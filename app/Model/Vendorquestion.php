<?php
App::uses('AppModel', 'Model');
/**
 * Vendorquestion Model
 *
 * @property User $User
 * @property Vendoranswer $Vendoranswer
 * @property Vendortest $Vendortest
 */
class Vendorquestion extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'group_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Vendoranswer' => array(
			'className' => 'Vendoranswer',
			'foreignKey' => 'vendorquestion_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Vendortest' => array(
			'className' => 'Vendortest',
			'joinTable' => 'vendortests_vendorquestions',
			'foreignKey' => 'vendorquestion_id',
			'associationForeignKey' => 'vendortest_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
