<?php
App::uses('AppModel', 'Model');

/**
 * Result Model
 *
 * @property Test $Test
 * @property User $User
 */
class Result extends AppModel
{


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Test' => array(
            'className' => 'Test',
            'foreignKey' => 'test_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * @param null $user_id
     * @param null $test_id
     * @return mixed
     */
    public function getMyPosition($user_id = null, $test_id = null)
    {
        $query = "SELECT user_id,mark, FIND_IN_SET( mark, (
        SELECT GROUP_CONCAT( mark
        ORDER BY mark DESC )
        FROM results  WHERE user_id =  '$user_id' AND test_id='$test_id')
        ) AS rank
        FROM results
        WHERE user_id =  '$user_id' AND test_id='$test_id'";
        $data = $this->query($query);
        //print_r($query);die;
        return $data;

    }

    public function getTop100($test_id = null, $limit = 100)
    {
        $query = "SELECT fullname, username, mark, FIND_IN_SET( mark, (
        SELECT GROUP_CONCAT( mark
        ORDER BY mark DESC )
        FROM results  WHERE test_id='$test_id')
        ) AS rank
        FROM results INNER JOIN users AS User ON User.id=results.user_id
        WHERE test_id='$test_id' ORDER BY rank ASC LIMIT $limit";
        $data = $this->query($query);

        $result = array();
        foreach ($data as $item) {
            $arr = [
                'username' => !empty($item['User']['fullname']) ? $item['User']['fullname'] : 'Anonymous User',
                'rank' => $item[0]['rank'],
                'mark' => $item['results']['mark'],
            ];
            $result[] = $arr;
        }
        //print_r($data);die;
        return $result;
    }
}
