<?php
App::uses('AppModel', 'Model');
/**
 * Vendoranswer Model
 *
 * @property Vendorquestion $Vendorquestion
 */
class Vendoranswer extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Vendorquestion' => array(
			'className' => 'Vendorquestion',
			'foreignKey' => 'vendorquestion_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
