<?php
App::uses('AppModel', 'Model');
/**
 * VendortestsVendorquestion Model
 *
 * @property Vendortest $Vendortest
 * @property Vendorquestion $Vendorquestion
 */
class VendortestsVendorquestion extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Vendortest' => array(
			'className' => 'Vendortest',
			'foreignKey' => 'vendortest_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Vendorquestion' => array(
			'className' => 'Vendorquestion',
			'foreignKey' => 'vendorquestion_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
