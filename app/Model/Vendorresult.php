<?php
App::uses('AppModel', 'Model');
/**
 * Vendorresult Model
 *
 * @property Vendortest $Vendortest
 * @property User $User
 */
class Vendorresult extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Vendortest' => array(
			'className' => 'Vendortest',
			'foreignKey' => 'vendortest_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
