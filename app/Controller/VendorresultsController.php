<?php
App::uses('AppController', 'Controller');
/**
 * Vendorresults Controller
 *
 * @property Vendorresult $Vendorresult
 * @property PaginatorComponent $Paginator
 */
class VendorresultsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Vendorresult->recursive = 0;
		$this->set('vendorresults', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Vendorresult->exists($id)) {
			throw new NotFoundException(__('Invalid vendorresult'));
		}
		$options = array('conditions' => array('Vendorresult.' . $this->Vendorresult->primaryKey => $id));
		$this->set('vendorresult', $this->Vendorresult->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Vendorresult->create();
			if ($this->Vendorresult->save($this->request->data)) {
				return $this->flash(__('The vendorresult has been saved.'), array('action' => 'index'));
			}
		}
		$vendortests = $this->Vendorresult->Vendortest->find('list');
		$users = $this->Vendorresult->User->find('list');
		$this->set(compact('vendortests', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Vendorresult->exists($id)) {
			throw new NotFoundException(__('Invalid vendorresult'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Vendorresult->save($this->request->data)) {
				return $this->flash(__('The vendorresult has been saved.'), array('action' => 'index'));
			}
		} else {
			$options = array('conditions' => array('Vendorresult.' . $this->Vendorresult->primaryKey => $id));
			$this->request->data = $this->Vendorresult->find('first', $options);
		}
		$vendortests = $this->Vendorresult->Vendortest->find('list');
		$users = $this->Vendorresult->User->find('list');
		$this->set(compact('vendortests', 'users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Vendorresult->id = $id;
		if (!$this->Vendorresult->exists()) {
			throw new NotFoundException(__('Invalid vendorresult'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Vendorresult->delete()) {
			return $this->flash(__('The vendorresult has been deleted.'), array('action' => 'index'));
		} else {
			return $this->flash(__('The vendorresult could not be deleted. Please, try again.'), array('action' => 'index'));
		}
	}
}
