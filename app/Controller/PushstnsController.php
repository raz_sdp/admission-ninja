<?php
App::uses('AppController', 'Controller');

/**
 * Pushstns Controller
 *
 * @property Pushstn $Pushstn
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class PushstnsController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'Flash');

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index()
    {
        $this->Pushstn->recursive = 0;
        $this->set('pushstns', $this->Paginator->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null)
    {
        if (!$this->Pushstn->exists($id)) {
            throw new NotFoundException(__('Invalid pushstn'));
        }
        $options = array('conditions' => array('Pushstn.' . $this->Pushstn->primaryKey => $id));
        $this->set('pushstn', $this->Pushstn->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */

    public function admin_add()
    {
        if ($this->request->is('post')) {
            $this->request->data['Pushstn']['message'] = strip_tags($this->request->data['Pushstn']['message'], '<sup>,<sub>');
            //pr($this->request->data);die;
            $this->Pushstn->create();
            if ($this->Pushstn->save($this->request->data)) {
//                $this->_send_global_push($this->request->data['Pushstn']);
                $this->Flash->success(__('The push has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The push could not be saved. Please, try again.'));
            }
        }
    }

    public function admin_send_nt($nt_id = null){
        $this->autoLayout->false;
        $this->Pushstn->recursive = -1;
        $query = [
            'conditions' => [
                'Pushstn.id' => $nt_id
            ]
        ];
        $msg = $this->Pushstn->find('first', $query);
        if(!empty($msg)){
            $this->_send_global_push($msg['Pushstn']);
            $this->Flash->success(__('The push has been send.'));
            return $this->redirect(array('action' => 'index'));
        }


    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null)
    {
        if (!$this->Pushstn->exists($id)) {
            throw new NotFoundException(__('Invalid pushstn'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Pushstn->save($this->request->data)) {
                $this->Flash->success(__('The pushstn has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The pushstn could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Pushstn.' . $this->Pushstn->primaryKey => $id));
            $this->request->data = $this->Pushstn->find('first', $options);
        }
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->Pushstn->id = $id;
        if (!$this->Pushstn->exists()) {
            throw new NotFoundException(__('Invalid pushstn'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Pushstn->delete()) {
            $this->Flash->success(__('The pushstn has been deleted.'));
        } else {
            $this->Flash->error(__('The pushstn could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
}
