<?php
App::uses('AppController', 'Controller');
/**
 * Vendorquestions Controller
 *
 * @property Vendorquestion $Vendorquestion
 * @property PaginatorComponent $Paginator
 */
class VendorquestionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($vendor_id = null) {
        $keyword = "";
        $conditions = array();
        $role = AuthComponent::user()['role'];

        if(!empty($this->request->data['keyword'])){
            $keyword = $this->request->data['keyword'];
        } elseif(!empty($this->params['named']['keyword'])){
            $keyword = $this->params['named']['keyword'];
        }elseif (!empty($this->params->query['keyword'])) {
            $keyword = $this->params->query['keyword'];
        }
        if($role=='cc')    $conditions = am($conditions, array('Vendorquestion.user_id' => AuthComponent::user()['id']));
        if (!empty($keyword)){
            $conditions = am($conditions,
                array(
                    'OR'=>
                        array(
                            'Vendorquestion.id LIKE' => '%' . $keyword . '%',
                            'Vendorquestion.question LIKE' => '%' . $keyword . '%',

                        ),
                )
            );
        }
        if(!empty($vendor_id)){
            $conditions = am(
                $conditions,
                array('Vendorquestion.user_id' => $vendor_id
                ));
        }
     $options = array(
            'conditions' => $conditions,
            'order' => 'Vendorquestion.id DESC',
            'contain' => [
                'Vendorquestion',
                'User'=>[
                    'fields' => ['User.company_name','User.email','User.username'],
                ],
                'Group' => [
                  'fields' => ['Group.title']
                ],
                'Vendoranswer' => [
                    'fields' => ['Vendoranswer.answer'],
                    'conditions' => ['Vendoranswer.is_correct' => 1]
                ]
            ]
        );
        $this->Vendorquestion->Behaviors->load('Containable');
        $this->Paginator->settings = $options;
        $this->Vendorquestion->recursive = 1;
        $this->set('questions', $this->Paginator->paginate('Vendorquestion'));

        $users = $this->Vendorquestion->User->find('list',['fields'=>'User.email','conditions'=>['User.role' => 'cc'],'order' => 'User.email ASC']);

        $this->set(compact('subjects','keyword','users','user_id'));
if ($role=='admin'){

		$this->Vendorquestion->recursive = 0;
		$this->set('vendorquestions', $this->Paginator->paginate());
}
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Vendorquestion->exists($id)) {
			throw new NotFoundException(__('Invalid vendorquestion'));
		}
		$options = array('conditions' => array('Vendorquestion.' . $this->Vendorquestion->primaryKey => $id));
		$this->set('vendorquestion', $this->Vendorquestion->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add($user_id = null) {
        if ($this->request->is('post')) {
            if(!empty($user_id)){
                $this->request->data['Vendorquestion']['user_id'] = $user_id;
            } else {
            $this->request->data['Vendorquestion']['user_id'] = AuthComponent::user()['id'];
            }
            $this->request->data['Vendorquestion']['question'] = preg_replace("/[\n\r]/","",strip_tags($this->request->data['Vendorquestion']['question'],'<sub>,<sup>'));
            $this->request->data['Vendorquestion']['answer_description'] = preg_replace("/[\n\r]/","",strip_tags($this->request->data['Vendorquestion']['answer_description'],'<sub>,<sup>'));
            #print_r($this->request->data);die;
            $this->Vendorquestion->create();
            if ($this->Vendorquestion->save($this->request->data)) {
                $answers = $this->request->data['Vendoranswer'];
                #pr($answers);die;
                if(!empty($answers)){
                    foreach($answers['answer'] as $key => $item){
                        $answer_data['Vendoranswer'] = array(
                            'vendorquestion_id' => $this->Vendorquestion->id,
                            'answer' => preg_replace("/[\n\r]/","",strip_tags($item,'<sub>,<sup>')),
                            'is_correct' => ++$key==$answers['is_correct'] ? 1 : null,
                        );
                        $this->Vendorquestion->Vendoranswer->create();
                        $this->Vendorquestion->Vendoranswer->save($answer_data);
                    }
                }

                $this->Session->setFlash(__('The question has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
                return $this->redirect(array('action' => 'add'));
            } else {
                $this->Session->setFlash(__('The question could not be saved. Please, try again.'));
            }
        }
		$groups = $this->Vendorquestion->Group->find('list');
		$vendortests = $this->Vendorquestion->Vendortest->find('list');
		$this->set(compact('groups', 'vendortests'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null, $vendor_id = null) {
        if (!$this->Vendorquestion->exists($id)) {
            throw new NotFoundException(__('Invalid question'));
        }
        if ($this->request->is(array('post', 'put'))) {
            //pr($this->request->data);die;
            $this->request->data['Vendorquestion']['question'] = strip_tags($this->request->data['Vendorquestion']['question'],'<sub>,<sup>');
            $this->request->data['Vendorquestion']['answer_description'] = strip_tags($this->request->data['Vendorquestion']['answer_description'],'<sub>,<sup>');
            if ($this->Vendorquestion->save($this->request->data)) {
                $this->Vendorquestion->Vendoranswer->deleteAll(['Vendoranswer.vendorquestion_id'=> $id], false);
                #print_r($this->request->data);die;
                $answers = $this->request->data['Vendoranswer'];

                if(!empty($answers)){
                    foreach($answers['answer'] as $key => $item){
                        $answer_data['Vendoranswer'] = array(
                            'vendorquestion_id' => $id,
                            'answer' => strip_tags($item,'<sub>,<sup>'),
                            'is_correct' => ++$key==$answers['is_correct'] ? 1 : null,
                        );
                        $this->Vendorquestion->Vendoranswer->create();
                        $this->Vendorquestion->Vendoranswer->save($answer_data);
                    }
                }
                $this->Session->setFlash(__('The question has been saved.'));
                return $this->redirect(array('action' => 'index', $vendor_id));
            } else {
                $this->Session->setFlash(__('The question could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Vendorquestion.' . $this->Vendorquestion->primaryKey => $id));
            $this->request->data = $this->Vendorquestion->find('first', $options);
        }
        $query = array(
            'recursive' => -1,
            'conditions' => array(
                'Vendoranswer.vendorquestion_id' => $id,
            ),
        );
        $no_of_questions = $this->getNumberList(10);
        $this->set(compact('no_of_questions'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null, $vendor_id = null) {
		$this->Vendorquestion->id = $id;
		if (!$this->Vendorquestion->exists()) {
			throw new NotFoundException(__('Invalid vendorquestion'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Vendorquestion->delete()) {
			return $this->flash(__('The vendorquestion has been deleted.'), array('action' => 'index', $vendor_id));
		} else {
			return $this->flash(__('The vendorquestion could not be deleted. Please, try again.'), array('action' => 'index'));
		}
	}

    public function admin_get_question($group_id = null){
        $this->autoLayout = false;
        $user_id = AuthComponent::user()['id'];
        $this->Vendorquestion->recursive = -1;
        $query = [
            'fields' => ['Vendorquestion.question'],
            'conditions' => [
                'Vendorquestion.group_id' => $group_id,
                'Vendorquestion.user_id' => $user_id
            ],
        ];
        $questions = $this->Vendorquestion->find('list',$query );
        $this->set(compact('questions'));
    }
}
