<?php
App::uses('AppController', 'Controller');
/**
 * Questions Controller
 *
 * @property Question $Question
 * @property PaginatorComponent $Paginator
 */
class QuestionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
 public function admin_index() {
    // Configure::write('debug',2);
        $keyword = "";
        $conditions = array();
        $role = AuthComponent::user()['role'];
        if(!empty($this->request->data['keyword'])){
            $keyword = $this->request->data['keyword'];
        } elseif(!empty($this->params['named']['keyword'])){
            $keyword = $this->params['named']['keyword'];
        }elseif (!empty($this->params->query['keyword'])) {
            $keyword = $this->params->query['keyword'];
        }
        if($role=='cw') {
            $conditions = am($conditions, array('Question.user_id' => AuthComponent::user()['id']));
        }
        if (!empty($keyword)){
            $conditions = am($conditions,
                array(
                    'OR'=>
                        array(
                            'Question.id LIKE' => '%' . $keyword . '%',
                            'Question.question LIKE' => '%' . $keyword . '%',
                            'User.fullname LIKE' => '%' . $keyword . '%',
                        ),
                )
            );
        }
             $subject_id = @$this->params->query['subject_id'];
             if(!empty($subject_id)){
                 $conditions = am($conditions,[
                     'Question.subject_id' => $subject_id
                 ]);
             } else {
                 $subject_id = '';
             }

     $user_id = @$this->params->query['user_id'];
     if(!empty($user_id)){
         $conditions = am($conditions,[
             'Question.user_id' => $user_id
         ]);
     } else {
         $user_id = '';
     }
        
        $options = array(
            'conditions' => $conditions,
            'order' => 'Question.id DESC',
            'contain' => [
                'Question',
                'Subject',
                'User'=>[
                    'fields' => ['User.fullname','User.email','User.username'],
                ],
                'Answer' => [
                    'fields' => ['Answer.answer'],
                    'conditions' => ['Answer.is_correct' => 1]
                ]
            ]
        );
        $this->Question->Behaviors->load('Containable');
        $this->Paginator->settings = $options;
		$this->Question->recursive = 1;
		$this->set('questions', $this->Paginator->paginate('Question'));
        $subjects = $this->Question->Subject->find('list',['order' => 'Subject.title ASC']);
        
         $users = $this->Question->User->find('list',['fields'=>'User.email','conditions'=>['User.role' => 'cw'],'order' => 'User.email ASC']);
         $this->loadModel(Group) ;
        $groups = $this->Group->find('list');
         $this->set(compact('subjects','keyword','subject_id','users','user_id','groups','board'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Question->exists($id)) {
			throw new NotFoundException(__('Invalid question'));
		}
		$options = array('conditions' => array('Question.' . $this->Question->primaryKey => $id));
		$this->set('question', $this->Question->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
            //print_r($this->request->data);die;
		    $subject_id = $this->request->data['Question']['subject_id'];
            $this->request->data['Question']['question'] = strip_tags($this->request->data['Question']['question'],'<sub>,<sup>');
            $this->request->data['Question']['answer_description'] = strip_tags($this->request->data['Question']['answer_description'],'<sub>,<sup>');
            #print_r($this->request->data);die;
			$this->Question->create();
			if ($this->Question->save($this->request->data)) {
                $answers = $this->request->data['Answer'];
                if(!empty($answers)){
                    foreach($answers['answer'] as $key => $item){
                        $answer_data['Answer'] = array(
                            'question_id' => $this->Question->id,
                            'answer' => strip_tags($item,'<sub>,<sup>'),
                            'feedback' => $answers['feedback'][$key],
                            'is_correct' => ++$key==$answers['is_correct'] ? 1 : null,
                        );
                        $this->Question->Answer->create();
                        $this->Question->Answer->save($answer_data);
                    }
                }

                $this->Session->setFlash(__('The question has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'add',$subject_id));
			} else {
				$this->Session->setFlash(__('The question could not be saved. Please, try again.'));
			}
		}
		$subjects = $this->Question->Subject->find('list',['order' => 'Subject.title ASC']);
		$this->Question->User->recursive = -1;
		$users = $this->Question->User->find('list',['fields' => 'User.email','conditions'=>['User.role' => 'cw']]);
        $this->loadModel(Group);
        $groups = $this->Group->find('list');

        $no_of_questions = $this->getNumberList(10);
		$this->set(compact('subjects', 'users','no_of_questions', 'groups'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Question->exists($id)) {
			throw new NotFoundException(__('Invalid question'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Question']['question'] = strip_tags($this->request->data['Question']['question'],'<sub>,<sup>');
            $this->request->data['Question']['answer_description'] = strip_tags($this->request->data['Question']['answer_description'],'<sub>,<sup>');
			if ($this->Question->save($this->request->data)) {
                $this->Question->Answer->deleteAll(['Answer.question_id'=> $id], false);
                #print_r($this->request->data);die;
                $answers = $this->request->data['Answer'];
                if(!empty($answers)){
                    foreach($answers['answer'] as $key => $item){
                        $answer_data['Answer'] = array(
                            'question_id' => $this->Question->id,
                            'answer' => strip_tags($item,'<sub>,<sup>'),
                            'feedback' => $answers['feedback'][$key],
                            'is_correct' => ++$key==$answers['is_correct'] ? 1 : null,
                        );
                        $this->Question->Answer->create();
                        $this->Question->id = $id;
                        $this->Question->Answer->save($answer_data);
                    }
                }
				$this->Session->setFlash(__('The question has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The question could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Question.' . $this->Question->primaryKey => $id));
			$this->request->data = $this->Question->find('first', $options);
		}
        $query = array(
            'recursive' => -1,
            'conditions' => array(
                'Answer.question_id' => $id,
            ),
        );
        #print_r($this->request->data);die;
        #$answers = $this->Question->Answer->find('all', $query);

		$subjects = $this->Question->Subject->find('list',['order' => 'Subject.title ASC']);
		$this->Question->User->recursive = -1;
		$users = $this->Question->User->find('list',['fields' => 'User.email','conditions'=>['User.role' => 'cw']]);
        $no_of_questions = $this->getNumberList(10);
        $this->set(compact('subjects', 'users','no_of_questions'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Question->id = $id;
		if (!$this->Question->exists()) {
			throw new NotFoundException(__('Invalid question'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Question->delete()) {
			$this->Session->setFlash(__('The question has been deleted.'));
		} else {
			$this->Session->setFlash(__('The question could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function admin_get_choice_lists(){
        $this->autoLayout = false;
        $question_id = $this->request->data['question_id'];
        $no_of_ans = $this->request->data['no_of_ans'];
        if(!empty($question_id)) {
            $query = array(
                'recursive' => -1,
                'conditions' => array(
                    'Answer.question_id' => $question_id,
                ),
                'limit' => $no_of_ans
            );
            $answers = $this->Question->Answer->find('all', $query);
            #AuthComponent::_setTrace($answers);
        }
        $this->set(compact('no_of_ans','answers'));
    }
    public function admin_get_questions_subject_wise($subject_id = null,$test_id = null){

        $this->autoLayout = false;
            $query_1 = [
                'fields' => ['Question.question'],
                'conditions' => [
                    'Question.subject_id' => $subject_id
                ],
            ];
            $questions = $this->Question->find('list', $query_1);
        $options = array('fields'=> array('TestsQuestion.question_id'),'conditions' => array('TestsQuestion.test_id'  => $test_id));
        $selected_questions = $this->Question->TestsQuestion->find('list', $options);
            $this->set(compact('questions','selected_questions'));
    }
    public function admin_get_question_subject_wise($user_id = null, $subject_id = null){
        //die;
        $conditions = array();
        if(!empty($subject_id)){
            $conditions = am($conditions, [
                'Question.subject_id' => $subject_id
            ]);
        }
        if(!empty($user_id)){
            $conditions = am($conditions, [
                'Question.user_id' => $user_id
            ]);
        }
        $query2 = [

            'conditions' => $conditions
        ];
        $this->Paginator->settings = $query2;
        $this->Question->recursive = 1;
        $this->set('questions', $this->Paginator->paginate('Question'));
    }
    public function admin_get_questions($subject_id = null){
        $query = [
            'conditions' => [
                'Question.subject_id' => $subject_id
            ]
        ];
        $this->Paginator->settings = $query;
        $this->Question->recursive = 1;
        $this->set('questions', $this->Paginator->paginate('Question'));
        $this->render('/questions/admin_get_question_subject_wise');
    }
}
