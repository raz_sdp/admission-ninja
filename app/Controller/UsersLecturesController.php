<?php
App::uses('AppController', 'Controller');
/**
 * UsersLectures Controller
 *
 * @property UsersLecture $UsersLecture
 * @property PaginatorComponent $Paginator
 */
class UsersLecturesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->UsersLecture->recursive = 0;
		$this->set('usersLectures', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->UsersLecture->exists($id)) {
			throw new NotFoundException(__('Invalid users lecture'));
		}
		$options = array('conditions' => array('UsersLecture.' . $this->UsersLecture->primaryKey => $id));
		$this->set('usersLecture', $this->UsersLecture->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->UsersLecture->create();
			if ($this->UsersLecture->save($this->request->data)) {
				$this->Flash->success(__('The users lecture has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The users lecture could not be saved. Please, try again.'));
			}
		}
		$users = $this->UsersLecture->User->find('list');
		$lectures = $this->UsersLecture->Lecture->find('list');
		$this->set(compact('users', 'lectures'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->UsersLecture->exists($id)) {
			throw new NotFoundException(__('Invalid users lecture'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UsersLecture->save($this->request->data)) {
				$this->Flash->success(__('The users lecture has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The users lecture could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UsersLecture.' . $this->UsersLecture->primaryKey => $id));
			$this->request->data = $this->UsersLecture->find('first', $options);
		}
		$users = $this->UsersLecture->User->find('list');
		$lectures = $this->UsersLecture->Lecture->find('list');
		$this->set(compact('users', 'lectures'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->UsersLecture->id = $id;
		if (!$this->UsersLecture->exists()) {
			throw new NotFoundException(__('Invalid users lecture'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UsersLecture->delete()) {
			$this->Flash->success(__('The users lecture has been deleted.'));
		} else {
			$this->Flash->error(__('The users lecture could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
    public function lecture_download_complete(){
        $this->autoRender = false;
        $this->autoLayout = false;
        if($this->request->is('post')) {
            $user_id = $this->request->data['UsersLecture']['user_id'];
            $lec_id = $this->request->data['UsersLecture']['lecture_id'];
            $this -> UsersLecture->recursive = -1;
            $data = $this->UsersLecture->findByUserIdAndLectureId($user_id, $lec_id);
            if(empty($data)) {
                $this->UsersLecture->create();
                $this->UsersLecture->save($this->request->data);
                die(json_encode(['success' => true]));
            } else {
                die(json_encode(['success' => false, 'msg' => 'This Lecture already Downloaded.']));
            }
        } else {
            die(json_encode(['success' => false, 'msg' => 'Invalid Request.']));
        }
    }
}
