<?php
App::uses('AppController', 'Controller');
/**
 * Devicetokens Controller
 *
 * @property Devicetoken $Devicetoken
 * @property PaginatorComponent $Paginator
 */
class DevicetokensController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Devicetoken->recursive = 0;
		$this->set('devicetokens', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Devicetoken->exists($id)) {
			throw new NotFoundException(__('Invalid devicetoken'));
		}
		$options = array('conditions' => array('Devicetoken.' . $this->Devicetoken->primaryKey => $id));
		$this->set('devicetoken', $this->Devicetoken->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Devicetoken->create();
			if ($this->Devicetoken->save($this->request->data)) {
				$this->Flash->success(__('The devicetoken has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The devicetoken could not be saved. Please, try again.'));
			}
		}
		$users = $this->Devicetoken->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Devicetoken->exists($id)) {
			throw new NotFoundException(__('Invalid devicetoken'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Devicetoken->save($this->request->data)) {
				$this->Flash->success(__('The devicetoken has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The devicetoken could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Devicetoken.' . $this->Devicetoken->primaryKey => $id));
			$this->request->data = $this->Devicetoken->find('first', $options);
		}
		$users = $this->Devicetoken->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Devicetoken->id = $id;
		if (!$this->Devicetoken->exists()) {
			throw new NotFoundException(__('Invalid devicetoken'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Devicetoken->delete()) {
			$this->Flash->success(__('The devicetoken has been deleted.'));
		} else {
			$this->Flash->error(__('The devicetoken could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
