<?php
App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property Devicetoken $Devicetoken
 * @property Transaction $Transaction
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property Group $Group
 * @property Test $Test
 *
 *
 */
class UsersController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index()
    {

        $keyword = "";
        $conditions = array();
        if (!empty($this->request->data['keyword'])) {
            $keyword = $this->request->data['keyword'];
        } elseif (!empty($this->params['named']['keyword'])) {
            $keyword = $this->params['named']['keyword'];
        } elseif (!empty($this->params->query['keyword'])) {
            $keyword = $this->params->query['keyword'];
        }
        if (!empty($keyword)) {
            $conditions = am($conditions,
                array(
                    'OR' =>
                        array(
                            'User.email LIKE' => '%' . $keyword . '%',
                            'User.username LIKE' => '%' . $keyword . '%',
                            'User.fullname LIKE' => '%' . $keyword . '%',
                            'User.city LIKE' => '%' . $keyword . '%',
                            'User.college LIKE' => '%' . $keyword . '%',
                        ),
                )
            );
        }
        $group = @$this->params->query['group'];
        if (!empty($group)) {
            $conditions = am($conditions, [
                'User.group' => $group
            ]);
        } else {
            $group = '';
        }
        $options = array(
            'conditions' => $conditions,
            'order' => 'User.id DESC',
        );
        $this->Paginator->settings = $options;
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate('User'));

        $this->loadModel('Group');
        $groups = $this->Group->find('list');
        $cc = [];
        if (@$this->params['pass']['0'] == 'cc') {
            $this->User->recursive = -1;
            $query = array(
                'conditions' => array(
                    'User.role' => 'cc'
                )
            );

            $this->Paginator->settings = $query;
            $cc = $this->Paginator->paginate('User');

        }
        $this->set(compact('users', 'keyword', 'group', 'groups', 'cc'));
        //pr($this->params);
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */

    /*	public function admin_add() {
            if ($this->request->is('post')) {
                //pr($this->request->data);die;
                $this->User->create();
                if ($this->User->save($this->request->data)) {
                    $this->Session->setFlash(__('The user has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
                }
            }
            $answers = $this->User->Answer->find('list');
            $this->set(compact('answers'));
        }*/
    public function admin_add($role = null)
    {
        if ($this->request->is('post')) {
            //pr($this->request->data);die;
            $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
            if (!empty($this->request->data['User']['filename']['name'])) {
                $file_name = $this->_upload($this->request->data['User']['filename']['name'], 'userimages');
                $this->request->data['User']['filename'] = $file_name;
            } else {
                unset($this->request->data['User']['filename']);
            }
            if (!empty($role)) $this->request->data['User']['role'] = $role;
            $this->User->create();
            $this->User->save($this->request->data);
            if (!empty($role))
                return $this->redirect(array('action' => 'index', $role));
            else return $this->redirect(array('action' => 'index'));
        }
        /*$answers = $this->User->Answer->find('list');
        $this->set(compact('answers'));*/
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null, $type = null)
    {

        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {

            if (!empty($this->request->data['User']['password'])) {
                $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
            } else {
                unset($this->request->data['User']['password']);
            }

            if (!empty($this->request->data['User']['filename']['name'])) {
                $file_name = $this->_upload($this->request->data['User']['filename'], 'userimages');
                $this->request->data['User']['filename'] = $file_name;
            } else {
                unset($this->request->data['User']['filename']);
            }
            $this->User->id = $id;
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been Updated.'));
                if ($type == 'cc') {
                    return $this->redirect(array('action' => 'index/cc'));
                } else {
                    return $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
        $answers = $this->User->Answer->find('list');
        $this->set(compact('answers'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null, $type = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->allowMethod('post', 'delete');


        $this->User->recursive = -1;
        $image = $this->User->findById($id);
        @unlink(WWW_ROOT . '/files/userimages/' . $image['User']['filename']);

        if ($this->User->delete()) {
            $this->Session->setFlash(__('The user has been deleted.'));
        } else {
            $this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
        }
        if ($type) {
            return $this->redirect(array('action' => 'index/cc'));

        } else {
            return $this->redirect(array('action' => 'index'));
        }
    }

    public function register()
    {
        //print_r($this->request->data);die;
        $this->autoLayout = false;
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $refer_code = $this->request->data['User']['refer_code'];
            $device_token = $this->request->data['Devicetoken']['title'];

            $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
            #$this->request->data['User']['password'] = $this->request->data['User']['password'];
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $user_id = $this->User->id;

                if (!empty($refer_code)) {
                    $this->User->Devicetoken->recursive = -1;
                    $exist_device = $this->User->Devicetoken->find('first', array('conditions' => ['Devicetoken.title' => $device_token]));
                    #Device token will save for the first time and rest time will update user data and is_login
                    $this->_save_devicetoken($device_token, $user_id);
                    if (empty($exist_device)) {
                    # Refer system point management
                        $this->_save_refer_point($refer_code, $user_id);


                    }
                }
                $this->User->recursive = -1;
                $user = $this->User->findById($user_id);

                $this->loadModel('Group');
                $group_id = $this->Group->findByTitle($user['User']['group'])['Group']['id'];
                $user['User']['group_id'] = $group_id;

                $image = !empty($user['User']['pic']) ? $user['User']['pic'] : 'default.png';
                $user['User']['pic'] = Router::url('/files/user_img/'.$image);


                die(json_encode(array('success' => true, 'userdata' => $user['User'])));
            }
        } else die(json_encode(array('success' => false, 'msg' => 'Invalid Request.')));
    }

    public function login()
    {
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $username = $this->request->data['User']['username'];
            $password = AuthComponent::password($this->request->data['User']['password']);
            $device_token = $this->request->data['Devicetoken']['title'];
            $this->User->recursive = -1;
            $is_user = $this->User->findByUsernameAndPassword($username, $password);

            //$is_user['User']['test_id'] = $test_id;

            if (!empty($is_user)) {
                $user_id = $is_user['User']['id'];
                $this->_save_devicetoken($device_token, $user_id);
                $this->loadModel('Group');
                // $this->loadModel('Test');
                $group_id = $this->Group->findByTitle($is_user['User']['group'])['Group']['id'];
                $is_user['User']['group_id'] = $group_id;

                $image = !empty($is_user['User']['pic']) ? $is_user['User']['pic'] : 'default.png';
                $is_user['User']['pic'] = Router::url('/files/user_img/'.$image);
                
                $this->Auth->login($is_user['User']);
                die(json_encode(array('success' => true, 'userdata' => $is_user['User'])));
            } else {
                die(json_encode(array('success' => false, 'msg' => 'Username and password is incorrect.')));
            }
        } else {
            die(json_encode(array('success' => false, 'msg' => 'Sorry!Inavalid request.')));
        }
    }

    public function validate_input($app = true)
    {
        $this->autoRender = false;
        $this->autoLayout = false;
        //echo $app; die;
        if ($app != 'false' || $app != 0) {
            $key = key($this->request->data['User']);
            $username = $this->request->data['User'][$key];
        } else {
            $key = key($this->params->query['data']['User']);
            $username = $this->params->query['data']['User'][$key];
        }

        /*if($key=='email') {
            if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
                return $this->response->statusCode(400);
            }
        }*/

        $this->User->recursive = -1;
        $is_exist = $this->User->find('first', [
            'conditions' => ['User.' . $key => $username]
        ]);
        if (!empty($is_exist)) {
            if ($app != 'false' || $app != 0) {
                die(json_encode(['success' => false, 'msg' => 'This username is already exist.']));
            } else {
                return $this->response->statusCode(400);
            }
        } else {
            if ($app != 'false' || $app != 0) {
                die(json_encode(['success' => true]));
            } else {
                return $this->response->statusCode(200);
            }
        }
    }

    public function dashboard()
    {
        Configure::write('debug', 2);
        $conditions = [];
        $role = AuthComponent::user()['role'];
        if ($role == 'cw') {
            $conditions = am($conditions, array('Question.user_id' => AuthComponent::user()['id']));
        }
        $query = [
            'conditions' => $conditions
        ];
        $questions = $this->User->Question->find('count', $query);
//        print_r($questions);
//        die;
        $this->loadModel('Test');
        $tests = $this->Test->find('count');
        $this->set(compact('questions', 'tests'));
        //$this->loadModel('Lecture');


    }

    public function admin_dashboard()
    {
        $role = AuthComponent::user()['role'];
        $id = AuthComponent::user()['id'];
        $conditions = [];
        if ($role == 'cw') {
            $conditions = am($conditions, array('Question.user_id' => $id));

            $query = [
                'conditions' => $conditions
            ];
            $questions = $this->User->Question->find('count', $query);
            $this->loadModel('Test');
            $tests = $this->Test->find('count');

        } else if ($role == 'cc') {
            $conditions = am($conditions, array('Vendorquestion.user_id' => $id));
            $query = [
                'conditions' => $conditions
            ];
            $questions = $this->User->Vendorquestion->find('count', $query);
            $conditions = am($conditions, array('Vendortest.user_id' => $id));
            $query = [
                'conditions' => array('Vendortest.user_id' => $id)
            ];
            $tests = $this->User->Vendortest->find('count', $query);
        } elseif ($role == 'admin') {
            $questions = $this->User->Question->find('count');
            $this->loadModel('Test');
            $tests = $this->Test->find('count');
            $users = $this->User->find('count');

        }
        $this->set(compact('questions', 'tests', 'users'));
    }

    public function admin_login()
    {
        $this->layout = 'login';
        if ($this->request->is('post')) {
            $password = AuthComponent::password($this->request->data['User']['password']);
            #$password = $this->request->data['User']['password'];
            #die($password);
            #print_r($this->request->data);die;
            $query = array(
                'conditions' => array(
                    'User.email' => $this->request->data['User']['email'],
                    'User.password' => $password,
                    'OR' => [
                        ['User.role' => 'admin'],
                        ['User.role' => 'cw'],
                        ['User.role' => 'cc'],
                    ],
                ),
            );
            //print_r($query);die;
            $this->User->recursive = -1;
            $is_exist = $this->User->find('first', $query);
            if (!empty($is_exist)) {
                /* $user_data = am(
                     $this->request->data['User'],
                     ['id' => $is_exist['User']['id']],
                     ['role' => 'admin']
                 );*/
                $this->Auth->login($is_exist['User']);
                $this->redirect(array('action' => 'dashboard', 'admin' => true));
            } else {
                $this->Auth->logout();
                $this->Session->setFlash(__('Incorrect username or password, please try again.'), 'default', array('class' => 'alert alert-danger text-center'));
            }
        }
    }

    public function admin_logout()
    {
        if (AuthComponent::user()['role'] == 'admin' || AuthComponent::user()['role'] == 'cw' || AuthComponent::user()['role'] == 'cc') {
            $this->Auth->logout();
            $this->redirect('/admin');
        } else {
            $this->Auth->logout();
            $this->redirect('/');
        }
    }

    /*API*/
    public function account_update()
    {
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autoRender = false;
        if ($this->request->is('post')) {
            if(empty($this->request->data['User']['password'])){
                unset($this->request->data['User']['password']);
            } else {
                $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
            }
            $user_id = $this->request->data['User']['id'];
            if(!empty($this->request->data['User']['pic'])) {
                $query = [
                    'conditions' => [
                        'User.id' => $user_id
                    ],
                    'recursive' => -1,
                    'fields' => [
                        'User.pic'
                    ]
                ];
                $user_pic = $this->User->find('first',$query);
                @unlink(WWW_ROOT.'/files/user_img/'.$user_pic['User']['pic']);
                $this->request->data['User']['pic'] = $this->_upload($this->request->data['User']['pic'], 'user_img');
            }            

            $this->User->id = $user_id;
            if ($this->User->save($this->request->data)) {
                $this->User->recursive = -1;
                $data = $this->User->findById($user_id);
                $image = !empty($data['User']['pic']) ? $data['User']['pic'] : 'img/avatar/default.png';
                $data['User']['pic'] = Router::url('/files/user_img/'.$image,true);
                $this->loadModel('Group');
                $group_id = $this->Group->findByTitle($data['User']['group'])['Group']['id'];
                $data['User']['group_id'] = $group_id;

                die(json_encode(['success' => true, 'msg' => 'Successfully Updated.', 'userdata' => $data['User']]));
            }
        }
    }

//    For User Profile
    public function profile($user_id = null)
    {
        if(empty($user_id)){
        $id = AuthComponent::user()['id'];
        } else {
            $id = $user_id;
        }
        $this->User->recursive = -1;
        $user = $this->User->findById($id);
        $this->loadModel(Group);
        $group = $this->Group->find('list');
        $this->set(compact('user', 'group'));
        if ($this->request->is(array('post', 'put'))) {

            if (!empty($this->request->data['User']['password'])) {
                $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
            } else {
                unset($this->request->data['User']['password']);
            }

            if (!empty($this->request->data['User']['filename']['name'])) {
                $file_name = $this->_upload($this->request->data['User']['filename'], 'userimages');
                $this->request->data['User']['filename'] = $file_name;
            } else {
                unset($this->request->data['User']['filename']);
            }
            $this->User->id = $id;
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('Profile has been updated.'), 'default', ['class' => 'text-center alert alert-success']);
                return $this->redirect(array('action' => 'profile'));

            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        }

    }

    public function status()
    {
        if ($this->request->is('post')) {
            $this->User->id = $this->request->data['id'];
            $status = $this->request->data['status'];
            if ($this->User->saveField('status', $status)) {
                die(json_encode(array('success' => true, 'msg' => 'change successful')));
            } else {
                die(json_encode(array('success' => false, 'msg' => 'Something went wrong')));
            }

        }
    }

    public function admin_contentwritter($user_id = null)
    {
        if (!empty($user_id)) {
            $query = [
                'recursive' => '-1',
                'conditions' => [
                    'User.id' => $user_id
                ]
            ];
            $user = $this->User->find('first', $query);
            $this->loadModel('Question');
            $query1 = [
                'recursive' => 0,
                'fields' => [
                    'Subject.id',
                    'Subject.university_id',
                    'Subject.title'
                ],
                'conditions' => [
                    'Question.user_id' => $user_id
                ],
                'group' => 'Subject.id',
                'limit' => 10,
            ];

            $this->Paginator->settings = $query1;
            $subjects = $this->Paginator->paginate('Question');
            //pr($subjects);die;
            $this->set(compact('subjects', 'user'));
        } else {
            $query = [
                'conditions' => [
                    'User.role' => 'cw'
                ]
            ];
            $this->User->recursive = -1;
            $this->Paginator->settings = $query;
            $cws = $this->Paginator->paginate('User');
            $this->set(compact('cws'));
        }
    }

    public function admin_get_username()
    {
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autoRender = false;
        $keyword = $this->params->query['term'];
        $query = [
            'recursive' => '-1',
            'fields' => [
                'User.id',
                'User.username',
                'User.pic'
            ],
            'conditions' => [
                'User.username LIKE' => '%' . $keyword . '%'
            ]
        ];
        $users = $this->User->find('all', $query);
        //pr($users);
        $result = Hash::extract($users, '{n}.User');
        //pr($result);die;
        echo json_encode($result);
        die;
    }

    /**
     * @param null $device
     * @param null $user_id
     * @param int $is_login
     *
     */
    private function _save_devicetoken($device = null, $user_id = null, $is_login = 1)
    {
        $this->User->Devicetoken->recursive = -1;
        $exist_device = $this->User->Devicetoken->find('first', array('conditions' => ['Devicetoken.title' => $device]));

        $data['Devicetoken'] = array(
            'user_id' => $user_id,
            'title' => $device,
            'is_login' => $is_login
        );

        if (empty($exist_device)) {
            $this->User->Devicetoken->create();
            $this->User->Devicetoken->save($data);
        } else {
            $this->User->Devicetoken->id = $exist_device['Devicetoken']['id'];
            $this->User->Devicetoken->save($data);
        }
    }

    private function _save_refer_point($refer_id = null, $user_id= null)
    {
        $this->User->Transaction->recursive = -1;
        $transaction_data = $this->User->Transaction->find('first', ['conditions' => ['Transaction.user_id' => $refer_id]]);
        if(!empty($transaction_data)){
        $total_point = 10 + @$transaction_data['Transaction']['total_point'];
        $available_point = 10 + @$transaction_data['Transaction']['available_balance'];
        $data['Transaction'] = [
            'total_point' => $total_point,
            'available_balance' => $available_point
        ];
        $this->User->Transaction->id = $transaction_data['Transaction']['id'];
        } else {
            $data['Transaction'] = [
                'user_id' => $refer_id,
                'total_point' => 10,
                'available_balance' => 10
                ];
            $this->User->Transaction->create();
        }
        $this->User->Transaction->save($data);

        #For who did sign up
        $transaction_data['Transaction'] = [
            'user_id' => $user_id,
            'total_point' => 10,
            'total_withdraw' => 0,
            'available_balance' => 10
        ];
        $this->User->Transaction->create();
        $this->User->Transaction->save($transaction_data);
    }
    public function point_withdraw(){
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autoRender = false;
        if($this->request->is('post')){
            $user_id = @$this->request->data['Transactionhistory']['user_id'];
            $amount = @$this->request->data['Transactionhistory']['amount'];
            $comment = @$this->request->data['Transactionhistory']['comment'];
            if(!empty($user_id) && !empty($amount)){
                $query = [
                    'conditions' => [
                        'Transaction.user_id' => $user_id
                    ],
                    'recursive' => -1
                ];
                $wallet = $this->User->Transaction->find('first', $query);
                if(!empty($wallet)){
                    if($wallet['Transaction']['available_balance'] >= $amount){
                        $available_point = @$wallet['Transaction']['available_balance'] - $amount ;
                        $withdraw = @$wallet['Transaction']['total_withdraw'] + $amount;
                        $transactionData['Transaction'] = [
                            'available_balance' => $available_point,
                            'total_withdraw' => $withdraw
                        ];
                        $this->User->Transaction->id = $wallet['Transaction']['id'];
                        $this->User->Transaction->save($transactionData);
                        $historyData['Transactionhistory'] = [
                            'user_id' => $user_id,
                            'amount' => $amount,
                            'comment' => $comment,
                            'status' => 1
                        ];
                        $this->User->Transactionhistory->create();
                        $this->User->Transactionhistory->save($historyData);
                        die(json_encode(array('success' => true, 'msg' => 'success!Your withdraw request under review .')));
                    } else {
                        die(json_encode(array('success' => false, 'msg' => 'Sorry!You don\'t have sufficiant balance .')));
                    }
                }else {
                    die(json_encode(array('success' => false, 'msg' => 'Sorry!You have not any balance.')));
                }
            }
        } else {
            die(json_encode(array('success' => false, 'msg' => 'Sorry!Something went wrong.')));
        }
    }
}
