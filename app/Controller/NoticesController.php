<?php
App::uses('AppController', 'Controller');
/**
 * Notices Controller
 *
 * @property Notice $Notice
 * @property PaginatorComponent $Paginator
 */
class NoticesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');


    public function get_notices(){
        $this->autoLayout = false;
        $this->autoRender = false;
        $data = $this->Notice->find('all');
        foreach($data as $notice){
            $notice_content[] = [
                'title' => @$notice['Notice']['title'],
                'url' => Router::url('/files/Notices/'.$notice['Notice']['filename'], true),
            ];
        }
        if(!empty($notice_content))
        die(json_encode(['success' => true, 'notices' => $notice_content]));
        else
        die(json_encode(['success' => false, 'msg' => 'No Notice Found.']));
    }
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Notice->recursive = 0;
		$this->set('notices', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Notice->exists($id)) {
			throw new NotFoundException(__('Invalid notice'));
		}
		$options = array('conditions' => array('Notice.' . $this->Notice->primaryKey => $id));
		$this->set('notice', $this->Notice->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
            if (!empty($this->request->data['Notice']['filename'][0]['name'])) {
                foreach ($this->request->data['Notice']['filename'] as $item) {
                    $file_name = $this->_upload($item, 'Notices');
                    if (!empty($file_name)) {
                        $this->request->data['Notice']['filename'] = $file_name;
                        $this->Notice->create();
                        if ($this->Notice->save($this->request->data)) {
                            $this->Session->setFlash(__('The notice has been saved.'));
                            return $this->redirect(array('action' => 'index'));
                        } else {
                            $this->Session->setFlash(__('The notice could not be saved. Please, try again.'));
                        }
                    }
                }
			
		    }
        }
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Notice->exists($id)) {
			throw new NotFoundException(__('Invalid notice'));
		}
		if ($this->request->is(array('post', 'put'))) {
            //print_r($this->request->data);die;
            if(!empty($this->request->data['Notice']['filename'])){
                $file_name = $this->_upload($this->request->data['Notice']['filename'],'Notices');
                $this->request->data['Notice']['filename'] = $file_name;
            } else{
                unset($this->request->data['Notice']['filename']);
            }
           // print_r($this->request->data);die;
            $this->Notice->id = $id;
            if ($this->Notice->save($this->request->data)) {
				$this->Session->setFlash(__('The notice has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The notice could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Notice.' . $this->Notice->primaryKey => $id));
			$this->request->data = $this->Notice->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Notice->id = $id;
		if (!$this->Notice->exists()) {
			throw new NotFoundException(__('Invalid notice'));
		}
		$this->request->allowMethod('post', 'delete');

        $this->Notice->recursive = -1;
        $image = $this->Notice->findById($id);
        @unlink(WWW_ROOT.'/files/Notices/'.$image['Notice']['filename']);


		if ($this->Notice->delete()) {
			$this->Session->setFlash(__('The notice has been deleted.'));
		} else {
			$this->Session->setFlash(__('The notice could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
