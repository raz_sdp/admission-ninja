<?php
App::uses('AppController', 'Controller');
/**
 * Transactionhistories Controller
 *
 * @property Transactionhistory $Transactionhistory
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class TransactionhistoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
        $query = [
            'recursive' => 0,
            'fields' => [
                'User.fullname',
                'User.id',
                'Transactionhistory.id',
                'Transactionhistory.amount',
                'Transactionhistory.comment',
                'Transactionhistory.status',
                'Transactionhistory.created'
            ]
        ];
        $this->Paginator->settings = $query;
        $transactionhistories = $this->Paginator->paginate('Transactionhistory');
        $this->set(compact('transactionhistories'));
//		$this->Transactionhistory->recursive = 0;
//		$this->set('transactionhistories', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($refer_code = null, $transaction_id = null) {
		$query = [
            'conditions' => [
                'User.refer_code' => $refer_code
            ],
            'recursive' => -1,
            'fields' =>[
                'User.id',
                'User.fullname',
                'User.group',
                'User.college',
                'User.created'
            ],
            'limit' => 5
        ];
        $this->Paginator->settings = $query;
        $total_refer = $this->Paginator->paginate('User');
        $this->loadModel('Transaction');
        $query1 = [
            'conditions' => [
                'Transaction.User_id' => $refer_code
            ],
            'recursive' => 0,
            'fields' => [
                'Transaction.total_point',
                'Transaction.total_withdraw',
                'Transaction.available_balance',
                'User.pic',
                'User.fullname',
                'User.created'
            ]
        ];
        $wallet = $this->Transaction->find('first', $query1);
        $query2 = [
            'conditions' => [
                'Transactionhistory.user_id' => $refer_code
            ],
            'recursive' => -1
        ];
        $totalWithdrowRequest = $this->Transactionhistory->find('count', $query2);
        $query3 = [
            'conditions' => [
                'Transactionhistory.id' => $transaction_id
            ],
            'recursive' => -1,
            'fields' => [
                'Transactionhistory.amount',
                'Transactionhistory.status',
                'Transactionhistory.mobile'
            ]
        ];
        $transaction_detail = $this->Transactionhistory->find('first', $query3);
        $this->set(compact('total_refer', 'wallet','totalWithdrowRequest','transaction_id','transaction_detail'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Transactionhistory->create();
			if ($this->Transactionhistory->save($this->request->data)) {
				$this->Flash->success(__('The transactionhistory has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The transactionhistory could not be saved. Please, try again.'));
			}
		}
		$users = $this->Transactionhistory->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Transactionhistory->exists($id)) {
			throw new NotFoundException(__('Invalid transactionhistory'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Transactionhistory->save($this->request->data)) {
				$this->Flash->success(__('The transactionhistory has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The transactionhistory could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Transactionhistory.' . $this->Transactionhistory->primaryKey => $id));
			$this->request->data = $this->Transactionhistory->find('first', $options);
		}
		$users = $this->Transactionhistory->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Transactionhistory->id = $id;
		if (!$this->Transactionhistory->exists()) {
			throw new NotFoundException(__('Invalid transactionhistory'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Transactionhistory->delete()) {
			$this->Flash->success(__('The transactionhistory has been deleted.'));
		} else {
			$this->Flash->error(__('The transactionhistory could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
    public function admin_paid_money($trans_id = null, $value = null){
        $this->request->data['Transactionhistory']['status'] = $value;
        $this->Transactionhistory->id = $trans_id;
        if($this->Transactionhistory->save($this->request->data)){
            die(json_encode(array('status' => true)));
        } else {
            die(json_encode(array('status' => false)));
        }
    }
}
