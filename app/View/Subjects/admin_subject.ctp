<?php
/**
 * Created by PhpStorm.
 * User: mukul
 * Date: 5/28/18
 * Time: 1:21 PM
 */ ?>
<?php echo $this->Form->input('subject_id', array(
        'label' => false,
        'class' => 'form-control col-md-3 js-subject',
        'options' => ['' =>'- None -'] + $subjects,
    )
); ?>
<?php  //pr($this->params) ?>
<script type="application/javascript">
    $('.js-subject').on('change', function () {
        var subject_id = $(this).val();
        $.get(ROOT + 'admin/questions/get_questions/' + subject_id, function (data) {
            $('.js-table-box').html(data);
        });
    });
</script>