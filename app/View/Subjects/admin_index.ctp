<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Subjects</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- box-Search -->
                    <div class="box-body">
                        <div class="row">
                            <?php echo $this->Form->create('Subject', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>
                            <div class="form-group" style="margin-left: 1px !important">
                                <div class="col-md-4">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="board" class="SubjectBoard" value="0" aria-label="..."  <?php echo $board =='0' ? 'checked' : '' ?> >
                                            বিষয়ভিত্তিক
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="board" class="SubjectBoard" value="1" aria-label="..."  <?php echo $board =='1' ? 'checked' : '' ?> >
                                            বিশ্ববিদ্যালয়ের বিগত বছরের প্রশ্ন ?
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="board" class="SubjectBoard" value="2" aria-label="..." <?php echo $board =='2' ? 'checked' : '' ?>>
                                            মডেল টেস্ট
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
<!--                                    <label class="col-sm-1 control-label">Group</label>-->
                                    <div class="col-sm-10">
                                        <?php echo $this->Form->input('group_id', array(
                                                'label' => false,
                                                'class' => 'form-control',
                                                'options' => ['' =>'Select Group'] + $groups,
                                                'value' => $group_id
                                            )
                                        ); ?>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
<!--                                    <label class="col-sm-2 control-label">University</label>-->
                                    <div class="col-sm-10">
                                        <?php echo $this->Form->input('university_id', array(
                                                'label' => false,
                                                'class' => 'form-control select2box',
                                                'options' => ['' =>'Select University'] + $universities,
                                                'value' => $university_id
                                            )
                                        ); ?>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3">
                                <div class="input-group input-group-sm">
                                    <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Search by Subject name"
                                           value="<?php echo $keyword ?>">

                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-1 input-group input-group-sm">
                               <span class="input-group-btn">
                                          <button type="submit" class="btn btn-info btn-flat">Go!</button>
                                        </span>
                            </div>
                            </form>
                            <!-- /.col -->
                        </div>
                        <!-- box-Search -->





                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th><?php echo $this->Paginator->sort('id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('group_id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('university_id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('title'); ?></th>
                                    <th><?php echo $this->Paginator->sort('No of Question'); ?></th>
                                    <th><?php echo $this->Paginator->sort('created'); ?></th>

                                    <th class="actions"><?php echo __('Actions'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($subjects as $subject): ?>
                                    <tr>
                                        <td><?php echo h($subject['Subject']['id']); ?>&nbsp;</td>
                                        <td>
                                            <?php echo $subject['Group']['title']; ?>
                                        </td>
                                        <td>
                                            <?php echo @$subject['University']['title']; ?>
                                        </td>
                                        <td><?php echo h($subject['Subject']['title']); ?>&nbsp;</td>
                                        <td><?php echo @AppHelper::countQuesSubwise($subject['Subject']['id']); ?>&nbsp;</td>
                                        <td><?php echo h($subject['Subject']['created']); ?>&nbsp;</td>

                                        <td class="actions">
                                            <a target="_blank" href="<?php echo $this->Html->URL('/admin/questions/index?subject_id='.$subject['Subject']['id']) ?>"><button class="btn btn-primary">View</button> </a>
                                            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $subject['Subject']['id']), ['target' => '_blank', 'class' => 'btn btn-warning']); ?>
                                            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $subject['Subject']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $subject['Subject']['id']), 'class' => 'btn btn-danger')); ?>
                                        </td>
                                    </tr>

                                <?php endforeach; ?>

                                </tbody>

                            </table>
                            <p class="pull-right">
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?>
                            </p>
                            <div class="clearfix"></div>
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <?php
                                echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                                echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                                echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                                ?>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>



