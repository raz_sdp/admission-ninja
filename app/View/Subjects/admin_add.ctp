<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Admin Add Subject</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php echo $this->Form->create('Subject'); ?>
                    <!--                    <form role="form">-->
                    <div class="box-body">
                        <div class="form-group">
                            <?php  echo $this->Form->input('group_id',['class'=>'form-control']);?>
                        </div>
                        <div class="form-group">
                            <?php
                            /*$options = array('M' => 'Male', 'F' => 'Female');
                            $attributes = array('legend' => false);
                            echo $this->Form->radio('board', $options, $attributes);*/
                            ?>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="data[Subject][board]" class="SubjectBoard" value="0" aria-label="...">
                                        বিষয়ভিত্তিক
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="data[Subject][board]" class="SubjectBoard" value="1" aria-label="..." checked>
                                        বিশ্ববিদ্যালয়ের বিগত বছরের প্রশ্ন ?
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="data[Subject][board]" class="SubjectBoard" value="2" aria-label="...">
                                        মডেল টেস্ট
                                    </label>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <?php  echo $this->Form->input('university_id',['class'=>'form-control select2box']);?>
                        </div>

                        <div class="form-group">
                            <?php  echo $this->Form->input('title',['class'=>'form-control']);?>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <?php /*echo $this->Form->end(__('Submit')); */?>
                    <!--                  </form>-->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
