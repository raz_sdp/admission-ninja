<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->

<section class="sidebar">

<!-- Sidebar user panel -->
<div class="user-panel">
    <div class="pull-left image">
        <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">-->
        <?php echo $this->Html->image(AuthComponent::user(['pic']), ['class' => 'img-circle', 'alt' => 'User Image']) ?>
    </div>
    <div class="pull-left info">
        <p> <?php echo(AuthComponent::user(['fullname'])); ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
</div>
<!-- search form -->

<!-- /.search form -->
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
<li class="header">MAIN NAVIGATION</li>

<!-- Manage Dashboard-->

<!--/Manage Dashboard-->
<?php
/*echo '<pre>';
print_r(AuthComponent::user()['role']);die;*/
$ctrl = strtolower($this->params['controller']);
$action = strtolower($this->params['action']);
$active = @($this->params[pass][0]);
if (AuthComponent::user()['role'] == 'admin'){
?>

<!--Manage Users-->
<li>
    <a href="<?php echo $this->Html->url("/dashboard", true) ?>">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
</li>
<li class="treeview <?php echo $ctrl == 'users' ? 'active menu-open' : '' ?>">
    <a href="#">
        <i class="fa fa-users"></i> <span>Manage Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo $ctrl == 'users' && $action == 'admin_add' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/users/add", true) ?>"><i
                    class="fa fa-user-plus"></i> Add New</a></li>
        <li class="<?php echo $ctrl == 'users' && $action == 'admin_index' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/users/index", true) ?>"><i
                    class="fa fa-list"></i>List</a></li>
        <li class="<?php echo $ctrl == 'users' && $action == 'admin_contentwritter' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/users/contentwritter", true) ?>"><i
                    class="fa fa-list"></i>List of cw</a></li>
    </ul>
</li>
<!--/Manage vender-->
<li class="treeview <?php echo $ctrl == 'user' ? 'active menu-open' : '' ?>">
    <a href="#">
        <i class="fa fa-users"></i> <span>Branch Manage</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo $ctrl == 'users' && $action == 'admin_add' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/users/add/cc", true) ?>"><i
                    class="fa fa-user-plus"></i> Add Vendor</a></li>
        <li class="<?php echo $ctrl == 'users' && $action == 'admin_index' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/users/index/cc", true) ?>"><i
                    class="fa fa-list"></i>Manage vendor</a></li>
    </ul>
</li>
<!--     Manage vendor question-->


<!-- Manage Lectures   -->

</li>
<li class="treeview <?php echo $ctrl == 'lectures' ? 'active menu-open' : '' ?> ">
    <a href="#">
        <i class="fa  fa-file"></i> <span>Manage Lectures</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo $ctrl == 'lectures' && $action == 'admin_add' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/lectures/add", true) ?>"><i
                    class="fa fa-plus"></i> Add New</a></li>
        <li class="<?php echo $ctrl == 'lectures' && $action == 'admin_index' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/lectures/index", true) ?>"><i
                    class="fa fa-list"></i>List</a></li>
    </ul>
</li>
<!-- /Manages Lectures-->


<!-- Manage Question   -->
<li class="treeview <?php echo $ctrl == 'questions' ? 'active menu-open' : '' ?>">
    <a href="#">
        <i class="fa fa-question-circle"></i> <span>Manage Question</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo $ctrl == 'questions' && $action == 'admin_add' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/questions/add", true) ?>"><i
                    class="fa fa-plus"></i> Add New</a></li>
        <li class="<?php echo $ctrl == 'questions' && $action == 'admin_index' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/questions/index", true) ?>"><i
                    class="fa fa-list"></i>List</a></li>
    </ul>
</li>
<!--/Manages Question-->

<!--Manages Subjects    -->
<li class="treeview <?php echo $ctrl == 'subjects' ? 'active menu-open' : '' ?> ">
    <a href="#">
        <i class="fa fa-book" aria-hidden="true"></i> <span>Manage Subjects</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo $ctrl == 'subjects' && $action == 'admin_add' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/subjects/add", true) ?>"><i
                    class="fa fa-plus"></i> Add New</a></li>
        <li class="<?php echo $ctrl == 'subjects' && $action == 'admin_index' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/subjects/index", true) ?>"><i
                    class="fa fa-list"></i>List</a></li>
    </ul>
</li>
<!--/Manages Subjects    -->
<li class="<?php echo $ctrl == 'settings' ? 'active' : '' ?>">
    <a href="<?php echo $this->Html->url("/admin/settings/edit/1", true) ?>">
        <i class="fa fa-question-circle"></i> <span>Manage Settings</span>
            <span class="pull-right-container">

            </span>
    </a>
</li>

<!--Manages Result   -->
<li class=" treeview <?php echo $ctrl == 'results' ? 'active menu-open' : '' ?> ">
    <a href="#">
        <i class="fa fa-list-alt"></i> <span>Manage Result</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo $ctrl == 'results' && $action == 'admin_add' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/results/add", true) ?>"><i
                    class="fa fa-plus"></i> Add New</a></li>
        <li class="<?php echo $ctrl == 'results' && $action == 'admin_index' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/results/index", true) ?>"><i
                    class="fa fa-list"></i>List</a></li>
    </ul>
</li>
<!--/Manages Result    -->

<!-- Manages Test    -->
<li class=" treeview <?php echo $ctrl == 'tests' ? 'active menu-open' : '' ?>">
    <a href="#">
        <i class="fa fa-question-circle"></i> <span>Manage Tests</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo $ctrl == 'tests' && $action == 'admin_add' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/tests/add", true) ?>"><i
                    class="fa fa-plus"></i> Add New</a></li>
        <li class="<?php echo $ctrl == 'tests' && $action == 'admin_index' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/tests/index", true) ?>"><i
                    class="fa fa-list"></i>List</a></li>
    </ul>
</li>

<!--/Manages Test    -->
<li class=" treeview <?php echo $ctrl == 'vendortests' ? 'active menu-open' : '' ?>">
    <a href="#">
        <i class="fa fa-question-circle"></i> <span>Manage Vendor Model Test</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo $ctrl == 'vendortests' && $action == 'admin_add' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/vendortests/add", true) ?>"><i
                    class="fa fa-plus"></i> Add</a></li>
        <li class="<?php echo $ctrl == 'vendortests' && $action == 'admin_index' ? 'active' : '' ?>"
        "><a href="<?php echo $this->Html->url("/admin/vendortests/index", true) ?>"><i
                class="fa fa-list"></i>List</a>
    </ul>
</li>


<!-- Manages Notices    -->
<li class=" treeview <?php echo $ctrl == 'notices' ? 'active menu-open' : '' ?>">
    <a href="#">
        <i class="fa fa-info"></i> <span>Manage Notices</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo $ctrl == 'notices' && $action == 'admin_add' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/notices/add", true) ?>"><i
                    class="fa fa-plus"></i> Add New</a></li>
        <li class="<?php echo $ctrl == 'notices' && $action == 'admin_index' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/notices/index", true) ?>"><i
                    class="fa fa-list"></i>List</a></li>
    </ul>
</li>

<!--/Manages Tests Question    -->


<li class=" treeview <?php echo $ctrl == 'tests_questions' ? 'active menu-open' : '' ?>">
    <a href="#">
        <i class="fa fa-question-circle"></i> <span>View question of Test</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
<!--        <li class="--><?php //echo $ctrl == 'tests_questions' && $action == 'assign_ques_into_test' ? 'active' : '' ?><!--"><a-->
<!--                href="--><?php //echo $this->Html->url("/admin/tests_questions/assign_ques_into_test", true) ?><!--"><i-->
<!--                    class="fa fa-plus"></i> Assign</a></li>-->
        <li class="<?php echo $ctrl == 'tests_questions' && $action == 'admin_index' ? 'active' : '' ?>"
        "><a href="<?php echo $this->Html->url("/admin/tests_questions/index", true) ?>"><i
                class="fa fa-list"></i>List</a>
    </ul>
</li>

</li>

<!--/Manages Tests Question    -->


<!--Manages Usersanswer    -->


<!--<li class="treeview <?php /*echo $ctrl=='usersanswers' ? 'active menu-open' : ''*/ ?>">
    <a href="#">
        <i class="fa fa-pencil-square-o"></i> <span>Manage Usersanswer</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php /*echo $ctrl=='usersanswer'&& $action=='admin_add' ? 'active' : ''*/ ?>""><a href="<?php /*echo $this->Html->url("/admin/Usersanswers/add", true) */ ?>"><i
                    class="fa fa-plus"></i> Add New</a></li>
        <li class="<?php /*echo $ctrl=='usersanswer'&& $action=='admin_index' ? 'active' : ''*/ ?>""><a href="<?php /*echo $this->Html->url("/admin/Usersanswers/index", true) */ ?>"><i
                    class="fa fa-list"></i>List</a></li>
    </ul>
</li>-->

<!--/Manages Usersanswer    -->

<!--Manages Emails    -->
<li class="treeview <?php echo $ctrl == 'emails' ? 'active menu-open' : '' ?>">
    <a href="#">
        <i class="fa fa-envelope"></i> <span>Manage Emails</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo $ctrl == 'emails' && $action == 'admin_add' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/emails/add", true) ?>"><i
                    class="fa fa-plus"></i> Add New</a></li>
        <li class="<?php echo $ctrl == 'emails' && $action == 'admin_index' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/emails/index", true) ?>"><i
                    class="fa fa-list"></i>List</a></li>
    </ul>
</li>

<!--/Manages Emails    -->


<!--Manages Push Notification    -->
<li class="treeview <?php echo $ctrl == 'pushstns' ? 'active menu-open' : '' ?>">
    <a href="#">
        <i class="fa fa-envelope"></i> <span>Manage Push Notification</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo $ctrl == 'pushstns' && $action == 'admin_add' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/pushstns/add", true) ?>"><i
                    class="fa fa-plus"></i> Add New</a></li>
        <li class="<?php echo $ctrl == 'pushstns' && $action == 'admin_index' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/pushstns/index", true) ?>"><i
                    class="fa fa-list"></i>List</a></li>
    </ul>
</li>

<!--/Manages Push Notification    -->

<!--Manages Group   -->
<li class=" treeview <?php echo $ctrl == 'groups' ? 'active menu-open' : '' ?>">
    <a href="#">
        <i class="fa fa-question-circle"></i> <span>Manage Group</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo $ctrl == 'groups' && $action == 'admin_add' ? 'active' : '' ?>"
        "><a href="<?php echo $this->Html->url("/admin/groups/add", true) ?>"><i
                class="fa fa-plus"></i> Add New</a>
</li>
<li class="<?php echo $ctrl == 'groups' && $action == 'admin_index' ? 'active' : '' ?>"
"><a href="<?php echo $this->Html->url("/admin/groups/index", true) ?>"><i
        class="fa fa-list"></i>List</a></li>
</ul>
</li>

<!--         for content writter-->

<?php
} else if (AuthComponent::user()['role'] == 'cw') {
    ?>

    <!--Manages Subjects    -->
    <li class="treeview <?php echo $ctrl == 'subjects' ? 'active menu-open' : '' ?> ">
        <a href="#">
            <i class="fa fa-book" aria-hidden="true"></i> <span>Manage Subjects</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo $ctrl == 'subjects' && $action == 'admin_add' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/subjects/add", true) ?>"><i
                        class="fa fa-plus"></i> Add New</a></li>
            <li class="<?php echo $ctrl == 'subjects' && $action == 'admin_index' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/subjects/index", true) ?>"><i
                        class="fa fa-list"></i>List</a></li>
        </ul>
    </li>
    <!--/Manages Subjects    -->

    <!-- Manage Question   -->
    <li class="treeview <?php echo $ctrl == 'questions' ? 'active menu-open' : '' ?>">
        <a href="#">
            <i class="fa fa-question-circle"></i> <span>Manage Question</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo $ctrl == 'questions' && $action == 'admin_add' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/questions/add", true) ?>"><i
                        class="fa fa-plus"></i> Add New</a></li>
            <li class="<?php echo $ctrl == 'questions' && $action == 'admin_index' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/questions/index", true) ?>"><i
                        class="fa fa-list"></i>List</a></li>
        </ul>
    </li>
    <!--/Manages Question-->


    <!-- Manages Test    -->
    <li class=" treeview <?php echo $ctrl == 'tests' ? 'active menu-open' : '' ?>">
        <a href="#">
            <i class="fa fa-question-circle"></i> <span>Manage Tests</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo $ctrl == 'tests' && $action == 'admin_add' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/tests/add", true) ?>"><i
                        class="fa fa-plus"></i> Add New</a></li>
            <li class="<?php echo $ctrl == 'tests' && $action == 'admin_index' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/tests/index", true) ?>"><i
                        class="fa fa-list"></i>List</a></li>
        </ul>
    </li>

    <!--/Manages Test    -->
    <li class=" treeview <?php echo $ctrl == 'tests_questions' ? 'active menu-open' : '' ?>">
        <a href="#">
            <i class="fa fa-question-circle"></i> <span>Assign questions into Test</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo $ctrl == 'tests_questions' && $action == 'assign_ques_into_test' ? 'active' : '' ?>">
                <a
                    href="<?php echo $this->Html->url("/admin/tests_questions/assign_ques_into_test", true) ?>"><i
                        class="fa fa-plus"></i> Assign</a></li>
            <li class="<?php echo $ctrl == 'tests_questions' && $action == 'admin_index' ? 'active' : '' ?>"
            "><a href="<?php echo $this->Html->url("/admin/tests_questions/index", true) ?>"><i
                    class="fa fa-list"></i>List</a>
    </li>
    </ul>
    </li>

    <!--Fior Get Bug list temporarly-->
    <li class=" treeview <?php echo $ctrl == 'universities' ? 'active menu-open' : '' ?>">
        <a href="#">
            <i class="fa fa-question-circle"></i> <span>Get My Bugs</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo $ctrl == 'universities' && $action == 'question_bank_bug_search' ? 'active' : '' ?>">
                <a
                    href="<?php echo $this->Html->url("/admin/universities/question_bank_bug_search/19", true) ?>" target="_blank"><i
                        class="fa fa-plus"></i> Get For Science</a>
            </li>

            <li class="">
                <a
                    href="<?php echo $this->Html->url("/admin/universities/question_bank_bug_search/20", true) ?>" target="_blank"><i
                        class="fa fa-plus"></i> Get For Business</a>
            </li>
            <li class="">
                <a
                    href="<?php echo $this->Html->url("/admin/universities/question_bank_bug_search/21", true) ?>" target="_blank"><i
                        class="fa fa-plus"></i> Get For Arts</a>
            </li>
    </ul>
    </li>


    <!--         for users-->
<?php
} else if (AuthComponent::user()['role'] == 'user') {
    ?>
    <li>
        <a href="<?php echo $this->Html->url("/student", true) ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>
    <!--    lectures-->
    </li>
    <li class="treeview <?php echo $ctrl == 'lectures' ? 'active menu-open' : '' ?> ">
        <a href="#">
            <i class="fa  fa-file"></i> <span>Lectures</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <?php
            $subjects = AppHelper::getSubjectLists(AuthComponent::user()['group_id']);
            $cnt = 1;
            foreach ($subjects as $key => $sub) {
                ?>
                <li class="<?php echo $ctrl == 'lectures' && $active == $key ? 'active' : '' ?>"><a
                        href="<?php echo $this->Html->url("/lectures/view/$key", true) ?>">

                        <?php echo $cnt++ . ' ' . $sub ?></a></li>
            <?php } ?>

        </ul>
    </li>



    <!-- Manage Question   -->
    <li class="treeview <?php echo $ctrl == 'questions' ? 'active menu-open' : '' ?>">
        <a href="#">
            <i class="fa fa-question-circle"></i> <span>Question Bank</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo $ctrl == 'questions' && $action == 'admin_add' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/questions/add", true) ?>"><i
                        class="fa fa-plus"></i> Add New</a></li>
            <li class="<?php echo $ctrl == 'questions' && $action == 'admin_index' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/questions/index", true) ?>"><i
                        class="fa fa-list"></i>List</a></li>
        </ul>
    </li>
    <!--/Manages Question-->


    <!--Manages Push Notification    -->
    <li class="treeview <?php echo $ctrl == 'pushstns' ? 'active menu-open' : '' ?>">
        <a href="#">
            <i class="fa fa-envelope"></i> <span>Model Test</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo $ctrl == 'pushstns' && $action == 'admin_add' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/pushstns/add", true) ?>"><i
                        class="fa fa-plus"></i> Add New</a></li>
            <li class="<?php echo $ctrl == 'pushstns' && $action == 'admin_index' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/pushstns/index", true) ?>"><i
                        class="fa fa-list"></i>List</a></li>
        </ul>
    </li>

    <!--/Manages Push Notification    -->

    <li class="treeview <?php echo $ctrl == 'pushstns' ? 'active menu-open' : '' ?>">
        <a href="#">
            <i class="fa fa-envelope"></i> <span>Notification</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo $ctrl == 'pushstns' && $action == 'admin_add' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/pushstns/add", true) ?>"><i
                        class="fa fa-plus"></i> Add New</a></li>
            <li class="<?php echo $ctrl == 'pushstns' && $action == 'admin_index' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/pushstns/index", true) ?>"><i
                        class="fa fa-list"></i>List</a></li>
        </ul>
    </li>




    <li class="treeview <?php echo $ctrl == 'pushstns' ? 'active menu-open' : '' ?>">
        <a href="#">
            <i class="fa fa-envelope"></i> <span>Tuition Media</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo $ctrl == 'pushstns' && $action == 'admin_add' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/pushstns/add", true) ?>"><i
                        class="fa fa-plus"></i> Add New</a></li>
            <li class="<?php echo $ctrl == 'pushstns' && $action == 'admin_index' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/pushstns/index", true) ?>"><i
                        class="fa fa-list"></i>List</a></li>
        </ul>
    </li>


    <li class="treeview <?php echo $ctrl == 'pushstns' ? 'active menu-open' : '' ?>">
        <a href="#">
            <i class="fa fa-list-alt"></i> <span>Result</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo $ctrl == 'pushstns' && $action == 'admin_add' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/pushstns/add", true) ?>"><i
                        class="fa fa-plus"></i> Add New</a></li>
            <li class="<?php echo $ctrl == 'pushstns' && $action == 'admin_index' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/pushstns/index", true) ?>"><i
                        class="fa fa-list"></i>List</a></li>
        </ul>
    </li>













    <!--            for vendor-->

<?php } else if (AuthComponent::user()['role'] == 'cc') { ?>
    <li>
        <a href="<?php echo $this->Html->url("/dashboard", true) ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>

    <!--    manage question-->
    <li class="treeview <?php echo $ctrl == 'vendorquestions' ? 'active menu-open' : '' ?>">
        <a href="#">
            <i class="fa fa-question-circle"></i> <span>Manage  Question</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo $ctrl == 'vendorquestions' && $action == 'admin_add' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/vendorquestions/add", true) ?>"><i
                        class="fa fa-plus"></i> Add New</a></li>
            <li class="<?php echo $ctrl == 'vendorquestions' && $action == 'admin_index' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/vendorquestions/index", true) ?>"><i
                        class="fa fa-list"></i>List</a></li>
        </ul>
    </li>

    <!--manage model test-->
    <li class=" treeview <?php echo $ctrl == 'vendortests' ? 'active menu-open' : '' ?>">
        <a href="#">
            <i class="fa fa-question-circle"></i> <span>Manage Model Test</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo $ctrl == 'vendortests' && $action == 'admin_add' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/vendortests/add", true) ?>"><i
                        class="fa fa-plus"></i> Add</a></li>
            <li class="<?php echo $ctrl == 'vendortests' && $action == 'admin_index' ? 'active' : '' ?>"
            "><a href="<?php echo $this->Html->url("/admin/vendortests/index", true) ?>"><i
                    class="fa fa-list"></i>List</a>
        </ul>
    </li>

    <!--       Assign questions into Test-->

    <li class="<?php echo $ctrl == 'vendorusers' ? 'active' : '' ?>">
        <a href="<?php echo $this->Html->url("/admin/vendorusers/add", true) ?>">
            <i class="fa fa-question-circle"></i> <span>Manage Group</span>
            <span class="pull-right-container">

            </span>
        </a>
    </li>




    <!--to See result-->
    <li class="treeview <?php echo $ctrl == 'vendorresults' ? 'active menu-open' : '' ?>">
        <a href="#">
            <i class="fa fa-list-alt"></i> <span>Result</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="<?php echo $ctrl == 'vendorresults' && $action == 'admin_index' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/vendorresults/index", true) ?>"><i
                        class="fa fa-plus"></i> List Of Model</a></li>
        </ul>
    </li>

<?php } ?>
</ul>
</section>
<!-- /.sidebar -->
</aside>