<div class="form-group">

    <?php  echo $this->Form->input('company_name',array('class'=>'form-control','placeholder'=>'Company Name'));?>

</div>
<div class="form-group">

    <?php  echo $this->Form->input('fullname',array('class'=>'form-control','placeholder'=>'Full Name'));?>

</div>
<div class="form-group">
    <?php  echo $this->Form->input('email',array('class'=>'form-control','placeholder'=>'Email'));?>
</div>
<div class="form-group">
    <?php  echo $this->Form->input('password',array('class'=>'form-control','placeholder'=>'Password'));?>
</div>
<div class="form-group">
    <?php  echo $this->Form->input('address',array('class'=>'form-control','placeholder'=>'Address'));?>
</div>
<div class="form-group">

    <?php  echo $this->Form->input('city',array('class'=>'form-control','placeholder'=>'City'));?>
</div>
<div class="form-group">

    <?php  echo $this->Form->input('mobile',array('class'=>'form-control','placeholder'=>'Mobile'));?>

</div>
<div class="form-group">

    <?php echo $this->Form->input('Logo',array('class'=>'form-control','type'=>'file','multiple'=>'multiple'));?>
</div>
<div class="checkbox">
    <label>

        <?php echo $this->Form->input('status');?>

    </label>
</div>
