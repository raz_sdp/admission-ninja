<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Setting</h3>
                    </div>



        <?php echo $this->Form->create('Setting'); ?>
                    <div class="form-group ">
                        <?php echo $this->Form->input('id');?>
                    </div>
     <div class="form-group">
	<?php echo $this->Form->input('is_free',['class' => 'checkbox-left']);?>
     </div>
                    <div class="col-md-12">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-block btn-primary btn-lg">Save</button>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    </form>

                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

