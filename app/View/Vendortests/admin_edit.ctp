<div class="vendortests form">
<?php echo $this->Form->create('Vendortest'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Vendortest'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('group_id');
		echo $this->Form->input('title');
		echo $this->Form->input('instructions');
		echo $this->Form->input('test_time');
		echo $this->Form->input('status');
		echo $this->Form->input('start_date');
		echo $this->Form->input('user_id');
		echo $this->Form->input('Vendorquestion');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Vendortest.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Vendortest.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Vendortests'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendorresults'), array('controller' => 'vendorresults', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendorresult'), array('controller' => 'vendorresults', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendorquestions'), array('controller' => 'vendorquestions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendorquestion'), array('controller' => 'vendorquestions', 'action' => 'add')); ?> </li>
	</ul>
</div>
