<div class="vendortests view">
<h2><?php echo __('Vendortest'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($vendortest['Vendortest']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Group'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vendortest['Group']['title'], array('controller' => 'groups', 'action' => 'view', $vendortest['Group']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($vendortest['Vendortest']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Instructions'); ?></dt>
		<dd>
			<?php echo h($vendortest['Vendortest']['instructions']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Test Time'); ?></dt>
		<dd>
			<?php echo h($vendortest['Vendortest']['test_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($vendortest['Vendortest']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start Date'); ?></dt>
		<dd>
			<?php echo h($vendortest['Vendortest']['start_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($vendortest['Vendortest']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($vendortest['Vendortest']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vendortest['User']['id'], array('controller' => 'users', 'action' => 'view', $vendortest['User']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Vendortest'), array('action' => 'edit', $vendortest['Vendortest']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Vendortest'), array('action' => 'delete', $vendortest['Vendortest']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $vendortest['Vendortest']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendortests'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendortest'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendorresults'), array('controller' => 'vendorresults', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendorresult'), array('controller' => 'vendorresults', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendorquestions'), array('controller' => 'vendorquestions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendorquestion'), array('controller' => 'vendorquestions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Vendorresults'); ?></h3>
	<?php if (!empty($vendortest['Vendorresult'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Vendortest Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Mark'); ?></th>
		<th><?php echo __('Exam Time'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($vendortest['Vendorresult'] as $vendorresult): ?>
		<tr>
			<td><?php echo $vendorresult['id']; ?></td>
			<td><?php echo $vendorresult['vendortest_id']; ?></td>
			<td><?php echo $vendorresult['user_id']; ?></td>
			<td><?php echo $vendorresult['mark']; ?></td>
			<td><?php echo $vendorresult['exam_time']; ?></td>
			<td><?php echo $vendorresult['created']; ?></td>
			<td><?php echo $vendorresult['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'vendorresults', 'action' => 'view', $vendorresult['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'vendorresults', 'action' => 'edit', $vendorresult['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'vendorresults', 'action' => 'delete', $vendorresult['id']), array('confirm' => __('Are you sure you want to delete # %s?', $vendorresult['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Vendorresult'), array('controller' => 'vendorresults', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Vendorquestions'); ?></h3>
	<?php if (!empty($vendortest['Vendorquestion'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Question'); ?></th>
		<th><?php echo __('Answer Description'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($vendortest['Vendorquestion'] as $vendorquestion): ?>
		<tr>
			<td><?php echo $vendorquestion['id']; ?></td>
			<td><?php echo $vendorquestion['user_id']; ?></td>
			<td><?php echo $vendorquestion['question']; ?></td>
			<td><?php echo $vendorquestion['answer_description']; ?></td>
			<td><?php echo $vendorquestion['created']; ?></td>
			<td><?php echo $vendorquestion['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'vendorquestions', 'action' => 'view', $vendorquestion['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'vendorquestions', 'action' => 'edit', $vendorquestion['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'vendorquestions', 'action' => 'delete', $vendorquestion['id']), array('confirm' => __('Are you sure you want to delete # %s?', $vendorquestion['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Vendorquestion'), array('controller' => 'vendorquestions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
