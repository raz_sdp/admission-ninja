<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Add Answer'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <?php echo $this->Form->create('Answer',array('type'=>'file')); ?>
                        <div class="box-body">
                            <div class="form-group">

                                <h3><?php echo $this->Form->input('question_id',array('class'=>'form-control'));?></h3>

                            </div>
                            <div class="form-group">

                                <label>Answer</label>
                                <textarea class="form-control" rows="6" placeholder="" name="data[Answer][answer]"> </textarea>

                            </div>
                            <div class="form-group">
                                <label>feedback</label>
                                <textarea class="form-control" rows="6" placeholder="" name="data[Answer][feedback]"></textarea>

                            </div>
                            <div class="form-group">

                                <?php echo $this->Form->input('is_correct',array('class'=>'form-control'));?>

                            </div>
                            <div class="form-group">

                                <?php echo $this->Form->input('User',array('class'=>'form-control'));?>

                            </div>

                        </div>

                        <?php echo $this->Form->end(__('Submit',['class'=>'Submit'])); ?>


                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>










<!--<div class="answers form">
<?php /*echo $this->Form->create('Answer'); */?>
	<fieldset>
		<legend><?php /*echo __('Admin Add Answer'); */?></legend>
	<?php
/*		echo $this->Form->input('question_id');
		echo $this->Form->input('answer');
		echo $this->Form->input('feedback');
		echo $this->Form->input('is_correct');
		echo $this->Form->input('User');
	*/?>
	</fieldset>
<?php /*echo $this->Form->end(__('Submit')); */?>
</div>
<div class="actions">
	<h3><?php /*echo __('Actions'); */?></h3>
	<ul>

		<li><?php /*echo $this->Html->link(__('List Answers'), array('action' => 'index')); */?></li>
		<li><?php /*echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); */?> </li>
		<li><?php /*echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); */?> </li>
	</ul>
</div>-->
