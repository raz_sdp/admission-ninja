<div class="pushstns view">
<h2><?php echo __('Pushstn'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($pushstn['Pushstn']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($pushstn['Pushstn']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Message'); ?></dt>
		<dd>
			<?php echo h($pushstn['Pushstn']['message']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($pushstn['Pushstn']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($pushstn['Pushstn']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Pushstn'), array('action' => 'edit', $pushstn['Pushstn']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Pushstn'), array('action' => 'delete', $pushstn['Pushstn']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $pushstn['Pushstn']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Pushstns'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pushstn'), array('action' => 'add')); ?> </li>
	</ul>
</div>
