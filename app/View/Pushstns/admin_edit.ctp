<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Admin Edit Push</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php echo $this->Form->create('Pushstn');
		            echo $this->Form->input('id');
                    ?>
                    <!--                    <form role="form">-->
                    <div class="box-body">
                        <div class="form-group">
                            <?php echo $this->Form->input('title', ['class' => 'form-control']); ?>
                        </div>

                        <div class="form-group">
                            <?php echo $this->Form->input('message', ['id' => 'editor', 'escape' => false]);?>
                            <label>Message</label>
                            <textarea cols="80" id="message" name="data[Pushstn][message]" rows="10"></textarea>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Update</button>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<script>
    CKEDITOR.replace('message');
</script>
