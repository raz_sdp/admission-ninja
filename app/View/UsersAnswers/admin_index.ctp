<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Lectures</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <th><?php echo $this->Paginator->sort('id'); ?></th>
                                <th><?php echo $this->Paginator->sort('user_id'); ?></th>
                                <th><?php echo $this->Paginator->sort('test_id'); ?></th>
                                <th><?php echo $this->Paginator->sort('question_id'); ?></th>
                                <th><?php echo $this->Paginator->sort('answer_id'); ?></th>
                                <th><?php echo $this->Paginator->sort('test_type'); ?></th>
                                <th><?php echo $this->Paginator->sort('ans_index'); ?></th>
                                <th><?php echo $this->Paginator->sort('is_flag'); ?></th>
                                <th><?php echo $this->Paginator->sort('ques_offset'); ?></th>
                                <th><?php echo $this->Paginator->sort('created'); ?></th>
                                <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </thead>
                            <tbody>
                            <?php foreach ($lectures as $lecture): ?>
                                <tr>
                                    <td><?php echo h($usersAnswer['UsersAnswer']['id']); ?>&nbsp;</td>
                                    <td>
                                        <?php echo $this->Html->link($usersAnswer['User']['name'], array('controller' => 'users', 'action' => 'view', $usersAnswer['User']['id'])); ?>
                                    </td>
                                    <td>
                                        <?php echo $this->Html->link($usersAnswer['Test']['title'], array('controller' => 'tests', 'action' => 'view', $usersAnswer['Test']['id'])); ?>
                                    </td>
                                    <td>
                                        <?php echo $this->Html->link($usersAnswer['Question']['id'], array('controller' => 'questions', 'action' => 'view', $usersAnswer['Question']['id'])); ?>
                                    </td>
                                    <td>
                                        <?php echo $this->Html->link($usersAnswer['Answer']['id'], array('controller' => 'answers', 'action' => 'view', $usersAnswer['Answer']['id'])); ?>
                                    </td>
                                    <td><?php echo h($usersAnswer['UsersAnswer']['test_type']); ?>&nbsp;</td>
                                    <td><?php echo h($usersAnswer['UsersAnswer']['ans_index']); ?>&nbsp;</td>
                                    <td><?php echo h($usersAnswer['UsersAnswer']['is_flag']); ?>&nbsp;</td>
                                    <td><?php echo h($usersAnswer['UsersAnswer']['ques_offset']); ?>&nbsp;</td>
                                    <td><?php echo h($usersAnswer['UsersAnswer']['created']); ?>&nbsp;</td>
                                    <td><?php echo h($usersAnswer['UsersAnswer']['modified']); ?>&nbsp;</td>
                                    <td class="actions">
                                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $usersAnswer['UsersAnswer']['id'])); ?>
                                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $usersAnswer['UsersAnswer']['id'])); ?>
                                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $usersAnswer['UsersAnswer']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $usersAnswer['UsersAnswer']['id']))); ?>
                                    </td>

                            <?php endforeach; ?>

                            </tbody>

                        </table>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                        </p>
                        <div class="clearfix"></div>
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <?php
                            echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                            echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                            echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                            ?>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>







<!--<div class="usersAnswers index">
	<h2><?php /*echo __('Users Answers'); */?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php /*echo $this->Paginator->sort('id'); */?></th>
			<th><?php /*echo $this->Paginator->sort('user_id'); */?></th>
			<th><?php /*echo $this->Paginator->sort('test_id'); */?></th>
			<th><?php /*echo $this->Paginator->sort('question_id'); */?></th>
			<th><?php /*echo $this->Paginator->sort('answer_id'); */?></th>
			<th><?php /*echo $this->Paginator->sort('test_type'); */?></th>
			<th><?php /*echo $this->Paginator->sort('ans_index'); */?></th>
			<th><?php /*echo $this->Paginator->sort('is_flag'); */?></th>
			<th><?php /*echo $this->Paginator->sort('ques_offset'); */?></th>
			<th><?php /*echo $this->Paginator->sort('created'); */?></th>
			<th><?php /*echo $this->Paginator->sort('modified'); */?></th>
			<th class="actions"><?php /*echo __('Actions'); */?></th>
	</tr>
	</thead>
	<tbody>
	<?php /*foreach ($usersAnswers as $usersAnswer): */?>
	<tr>
		<td><?php /*echo h($usersAnswer['UsersAnswer']['id']); */?>&nbsp;</td>
		<td>
			<?php /*echo $this->Html->link($usersAnswer['User']['name'], array('controller' => 'users', 'action' => 'view', $usersAnswer['User']['id'])); */?>
		</td>
		<td>
			<?php /*echo $this->Html->link($usersAnswer['Test']['title'], array('controller' => 'tests', 'action' => 'view', $usersAnswer['Test']['id'])); */?>
		</td>
		<td>
			<?php /*echo $this->Html->link($usersAnswer['Question']['id'], array('controller' => 'questions', 'action' => 'view', $usersAnswer['Question']['id'])); */?>
		</td>
		<td>
			<?php /*echo $this->Html->link($usersAnswer['Answer']['id'], array('controller' => 'answers', 'action' => 'view', $usersAnswer['Answer']['id'])); */?>
		</td>
		<td><?php /*echo h($usersAnswer['UsersAnswer']['test_type']); */?>&nbsp;</td>
		<td><?php /*echo h($usersAnswer['UsersAnswer']['ans_index']); */?>&nbsp;</td>
		<td><?php /*echo h($usersAnswer['UsersAnswer']['is_flag']); */?>&nbsp;</td>
		<td><?php /*echo h($usersAnswer['UsersAnswer']['ques_offset']); */?>&nbsp;</td>
		<td><?php /*echo h($usersAnswer['UsersAnswer']['created']); */?>&nbsp;</td>
		<td><?php /*echo h($usersAnswer['UsersAnswer']['modified']); */?>&nbsp;</td>
		<td class="actions">
			<?php /*echo $this->Html->link(__('View'), array('action' => 'view', $usersAnswer['UsersAnswer']['id'])); */?>
			<?php /*echo $this->Html->link(__('Edit'), array('action' => 'edit', $usersAnswer['UsersAnswer']['id'])); */?>
			<?php /*echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $usersAnswer['UsersAnswer']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $usersAnswer['UsersAnswer']['id']))); */?>
		</td>
	</tr>
<?php /*endforeach; */?>
	</tbody>
	</table>
	<p>
	<?php
/*	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	*/?>	</p>
	<div class="paging">
	<?php
/*		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	*/?>
	</div>
</div>
<div class="actions">
	<h3><?php /*echo __('Actions'); */?></h3>
	<ul>
		<li><?php /*echo $this->Html->link(__('New Users Answer'), array('action' => 'add')); */?></li>
		<li><?php /*echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); */?> </li>
		<li><?php /*echo $this->Html->link(__('List Tests'), array('controller' => 'tests', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Test'), array('controller' => 'tests', 'action' => 'add')); */?> </li>
		<li><?php /*echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); */?> </li>
		<li><?php /*echo $this->Html->link(__('List Answers'), array('controller' => 'answers', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Answer'), array('controller' => 'answers', 'action' => 'add')); */?> </li>
	</ul>
</div>-->
