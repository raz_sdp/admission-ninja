<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Add Users Answer'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <?php echo $this->Form->create('UsersAnswer',array('type'=>'file')); ?>
                        <div class="box-body">
                            <div class="form-group">

                                <h3><?php echo $this->Form->input('user_id',array('class'=>'form-control'));?></h3>

                            </div>
                            <div class="form-group">

                                <?php echo $this->Form->input('test_id',array('class'=>'form-control'));?>

                            </div>
                            <div class="form-group">

                                <?php echo $this->Form->input('question_id',array('class'=>'form-control'));?>

                            </div>
                            <div class="form-group">

                                <?php echo $this->Form->input('answer_id',array('class'=>'form-control'));?>

                            </div>
                            <div class="form-group">

                                <?php echo $this->Form->input('test_type',array('class'=>'form-control'));?>

                            </div>
                            <div class="form-group">

                                <?php echo $this->Form->input('ans_index',array('class'=>'form-control'));?>

                            </div> <div class="form-group">

                                <?php echo $this->Form->input('is_flag',array('class'=>''));?>

                            </div> <div class="form-group">

                                <?php echo $this->Form->input('ques_offset',array('class'=>'form-control'));?>

                            </div>

                        </div>

                        <?php echo $this->Form->end(__('Submit',['class'=>'Submit'])); ?>


                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>









<!--
<div class="usersAnswers form">
<?php /*echo $this->Form->create('UsersAnswer'); */?>
	<fieldset>
		<legend><?php /*echo __('Admin Add Users Answer'); */?></legend>
	<?php
/*		echo $this->Form->input('user_id');
		echo $this->Form->input('test_id');
		echo $this->Form->input('question_id');
		echo $this->Form->input('answer_id');
		echo $this->Form->input('test_type');
		echo $this->Form->input('ans_index');
		echo $this->Form->input('is_flag');
		echo $this->Form->input('ques_offset');
	*/?>
	</fieldset>
<?php /*echo $this->Form->end(__('Submit')); */?>
</div>
<div class="actions">
	<h3><?php /*echo __('Actions'); */?></h3>
	<ul>

		<li><?php /*echo $this->Html->link(__('List Users Answers'), array('action' => 'index')); */?></li>
		<li><?php /*echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); */?> </li>
		<li><?php /*echo $this->Html->link(__('List Tests'), array('controller' => 'tests', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Test'), array('controller' => 'tests', 'action' => 'add')); */?> </li>
		<li><?php /*echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); */?> </li>
		<li><?php /*echo $this->Html->link(__('List Answers'), array('controller' => 'answers', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Answer'), array('controller' => 'answers', 'action' => 'add')); */?> </li>
	</ul>
</div>
-->



<!--<div class="usersAnswers form">
<?php /*echo $this->Form->create('UsersAnswer'); */?>
	<fieldset>
		<legend><?php /*echo __('Admin Add Users Answer'); */?></legend>
	<?php
/*		echo $this->Form->input('user_id');
		echo $this->Form->input('test_id');
		echo $this->Form->input('question_id');
		echo $this->Form->input('answer_id');
		echo $this->Form->input('test_type');
		echo $this->Form->input('ans_index');
		echo $this->Form->input('is_flag');
		echo $this->Form->input('ques_offset');
	*/?>
	</fieldset>
<?php /*echo $this->Form->end(__('Submit')); */?>
</div>
<div class="actions">
	<h3><?php /*echo __('Actions'); */?></h3>
	<ul>

		<li><?php /*echo $this->Html->link(__('List Users Answers'), array('action' => 'index')); */?></li>
		<li><?php /*echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); */?> </li>
		<li><?php /*echo $this->Html->link(__('List Tests'), array('controller' => 'tests', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Test'), array('controller' => 'tests', 'action' => 'add')); */?> </li>
		<li><?php /*echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); */?> </li>
		<li><?php /*echo $this->Html->link(__('List Answers'), array('controller' => 'answers', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Answer'), array('controller' => 'answers', 'action' => 'add')); */?> </li>
	</ul>
</div>
-->