<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Add Notice'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <?php echo $this->Form->create('Notice',array('type'=>'file')); ?>
                        <div class="box-body">
                            <div class="form-group">

                                <?php   echo $this->Form->input('title',['class'=>'form-control']);?>

                            </div>

                            <div class="form-group">

                                <label for="exampleInputFullname">Input File</label>
                                <?php echo $this->Form->input('filename.',array('type'=>'file','multiple'=>'multiple'));?>

                            </div>

                        </div>

                        <?php echo $this->Form->end(__('Submit',['class'=>'Submit'])); ?>


                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>







<!--<div class="notices form">
<?php /*echo $this->Form->create('Notice',['type'=>'file']); */?>
	<fieldset>
		<legend><?php /*echo __('Admin Edit Notice'); */?></legend>
	<?php
/*		echo $this->Form->input('id');
		echo $this->Form->input('title');
    echo $this->Form->input('filename',array('type'=>'file'));
	*/?>
	</fieldset>
<?php /*echo $this->Form->end(__('Submit')); */?>
</div>
<div class="actions">
	<h3><?php /*echo __('Actions'); */?></h3>
	<ul>

		<li><?php /*echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Notice.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Notice.id'))); */?></li>
		<li><?php /*echo $this->Html->link(__('List Notices'), array('action' => 'index')); */?></li>
	</ul>
</div>-->
