<style type="text/css">
    .navbar-fixed-bottom .navbar-collapse, .navbar-fixed-top .navbar-collapse {
        max-height: 369px !important;
    }
</style>

<!-- =========================
    START ABOUT US SECTION
============================== -->


<section class="about page" id="ABOUT">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <!-- ABOUT US SECTION TITLE-->
                <div class="section_title">
                    <h2>About App</h2>

                    <p>Admission Ninja - একটি মোবাইল এ্যাপ যা হতে যাচ্ছে আমাদের দেশের বিশ্ববিদ্যলয়ের ভর্তি পরীক্ষার
                        সম্পূর্ণ এবং কোচিং এর বিকল্প সমাধান । এই এ্যাপ বাবহারের মাধ্যমে শিক্ষার্থীরা বাসা অথবা যেকোন
                        জায়গা হতে প্রস্তুতি নিতে পারবে । যার ফলে তারা বেশি সময় পাবে, সেই সাথে প্রস্তুতির জন্য
                        দুশ্চিন্তামুক্ত থাকবেন অভিভাবকরা, যেহেতু শিক্ষার্থীদের কোচিং এর জন্য বাইরে বের হতে হয়, সেজন্য
                        যানজটের জন্যেও তাদের ভুমিকা কম নয় । এখন ঘরে বসে প্রস্তুতি নেয়ার জন্য যানজট কিছুটা হলেও কমে
                        যাচ্ছে ।
                    </p>
                </div>
            </div>

        </div>
    </div>
    <div class="inner_about_area">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                        <!-- PHONE -->
                        <img src="images/about_iphone.png" alt="">
                    </div>
                </div>
                <div class="col-md-6  wow fadeInRight" data-wow-duration="1s" data-wow-delay=".5s">
                    <!-- TITLE -->
                    <div class="inner_about_title">
                        <h2>Why this is best for you</h2>

                        <p>সর্বোচ্চ সংখ্যক প্রতিযোগীর সাথে প্রতিযোগিতা করে , প্রতিযোগিতায় টিকে থাকার অব্যার্থ্য অস্ত্রের
                            কারনে Admission Ninja আছে আপনার জন্য একমাত্র নির্ভরতার প্রতীক । উদাহরণ সরূপ বলা যায় , আপনি
                            একটি কোচিং সেন্টারের মাধ্যমে সর্বোচ্চ ৫০০- ৫,০০০ ছাত্র-ছাত্রীর সাথে প্রতিযোগিতা করতে পারেন ,
                            কিন্তু বাস্তবিক পক্ষে বিশ্ববিদ্যালয় ভর্তি পরীক্ষায় আপনাকে নূন্যতম ৫০,০০০ - ৫ লক্ষ
                            ছাত্র-ছাত্রীর সাথে প্রতিযোগিতা করতে হয় । যার ফলে আপনি কোচিং সেন্টারে Model Test দিয়ে
                            সারাদেশের মধ্যো আপনার প্রকৃত অবস্থান জানতে পারেননা । Admission Ninja is here to solve the
                            problem.</p>
                    </div>
                    <div class="inner_about_desc">

                        <!-- SINGLE DESC -->
                        <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="1s">
                            <!-- ICON -->
                            <div><i class="pe-7s-help1"></i></div>
                            <!-- HEADING DESCRIPTION -->
                            <h3>মডেল টেস্ট</h3>

                            <p>এই মডেল টেস্ট এ অংশগ্রহণ করে আপনি সর্বোচ্চ সংখ্যক ছাত্র-ছাত্রীরদের সাথে প্রতিযোগিতা করে
                                সারাদেশে আপনার প্রকৃত অবস্থান জানতে পারবেন, বুঝতে পারবেন আপনার প্রস্তুতি আসলেই চান্স
                                পাবার জন্য যথেষ্ট কিনা । </p>
                        </div>

                        <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="2s">
                            <!-- ICON -->
                            <div><i class="pe-7s-stopwatch"></i></div>
                            <!-- HEADING DESCRIPTION -->
                            <h3>সম্পূর্ণ ফ্রী </h3>

                            <p>লেকচার শিট, ভিডিও টিউটোরিয়াল ফ্রী এবং অফলাইনে পড়তে পারবেন । শুধু তাই নয় অনুশীলনের জন্য
                                আছে Question Bank যা আপনার প্রস্তুতিকে আরো এক ধাপ এগিয়ে নিয়ে যাবে । </p>
                        </div>
                        <!-- END SINGLE DESC -->

                        <!-- SINGLE DESC -->

                        <!-- END SINGLE DESC -->

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="inner_about_area">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="1s">
                        <!-- ICON -->
                        <div><i class="pe-7s-timer"></i></div>
                        <!-- HEADING DESCRIPTION -->
                        <h3>সঠিক দিক নির্দেশনা </h3>

                        <p>আপনি যদি উচ্চ মাধ্যমিকের ছাত্র-ছাত্রী হয়ে থাকেন তাহলে আপনাকে সর্বপ্রথম জানতে হবে
                            বিশ্ববিদ্যালয়ের ভর্তি পরীক্ষা পদ্ধতি, যোগ্যতা এবং কোন সাবজেক্ট পড়লে কেমন ক্যারিয়ার গড়া যায় ।
                            ভাল ফলাফল করেও যদি আপনি ভাল কোন বিশ্ববিদ্যালয় বা মেডিকেল কলেজে ভর্তি হবার সুযোগ না পান তাহলে
                            সে ফলাফল বৃথা । সঠিক দিকনির্দেশনাই পারে ভাল একটি বিশ্ববিদ্যালয়ে ভাল একটি বিষয়ে পড়ে খুব ভাল
                            ক্যারিয়ার গড়ার সাহায্য করতে।</p>
                    </div>

                </div>
                <div class="col-md-6  wow fadeInRight" data-wow-duration="1s" data-wow-delay=".5s">
                    <!-- TITLE -->

                    <div class="inner_about_desc">

                        <!-- SINGLE DESC -->

                        <!-- END SINGLE DESC -->

                        <!-- SINGLE DESC -->
                        <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="2s">
                            <!-- ICON -->
                            <div><i class="pe-7s-stopwatch"></i></div>
                            <!-- HEADING DESCRIPTION -->
                            <h3>লাইভ সাপোট্</h3>

                            <p>অভিজ্ঞ শিক্ষক দ্বারা শিক্ষাথীদের প্রশ্নের উত্তর দেয়া হয় । যাতে করে তারা লেকচার কনটেন্ট্
                                ভালভাবে বুঝতে পারে । </p>
                        </div>


                        <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="2s">
                            <!-- ICON -->
                            <div><i class="pe-7s-stopwatch"></i></div>
                            <!-- HEADING DESCRIPTION -->
                            <h3>ভ্রাম্যমান প্রস্তুতি</h3>

                            <p>যেকোন জায়গা হতে যেকোন সময় আপনি প্রস্তুতি নিতে পারবেন । আর তাই নিজ বাড়ি ছেড়ে ঢাকা বা অন্য
                                শহরে গিয়ে বসবাস করার প্রয়োজন হয়না বলে জীবন ধারন খরচ কমে যাই । </p>
                        </div>
                        <!-- END SINGLE DESC -->

                        <!-- SINGLE DESC -->

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="video_area">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInLeftBig">
                    <!-- VIDEO LEFT TITLE -->
                    <div class="video_title">
                        <h2>Best App in the market</h2>

                        <p>আপনি একটি কোচিং সেন্টারের মাধ্যমে সর্বোচ্চ ৫০০- ৫,০০০ ছাত্র-ছাত্রীর সাথে প্রতিযোগিতা করতে
                            পারেন , কিন্তু বাস্তবিক পক্ষে বিশ্ববিদ্যালয় ভর্তি পরীক্ষায় আপনাকে নূন্যতম ৫০,০০০ - ৫ লক্ষ
                            ছাত্র-ছাত্রীর সাথে প্রতিযোগিতা করতে হয় । যার ফলে আপনি কোচিং সেন্টারে Model Test দিয়ে
                            সারাদেশের মধ্যো আপনার প্রকৃত অবস্থান জানতে পারেননা । Admission Ninja is best for providing
                            the most important questions as Model Test.</p>
                    </div>
                    <div class="video-button">
                        <!-- BUTTON -->
                        <a class="btn btn-primary btn-video" href="#FEATURES" role="button">Features</a>
                    </div>
                </div>
                <div class="col-md-6 wow fadeInRightBig col-sm-12 col-xs-12">
                    <!-- VIDEO -->
                    <div class="video">
                        <iframe
                            src="<?php echo $this->Html->url('/files/video/admission_ninja_promo_video.mp4', true)?>"
                            width="560" height="315"></iframe>
                        <!--<video width="560" height="315" controls autoplay="autoplay">
                            <source src="<?php /*echo $this->Html->url('/files/video/admission_ninja_promo_video.mp4', true)*/?>" type="video/mp4">
                            <source src="<?php /*echo $this->Html->url('/files/video/admission_ninja_promo_video.mp4', true)*/?>" type="video/ogg">
                            Your browser does not support HTML5 video.
                        </video>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End About Us -->


<!-- =========================
    START TESTIMONIAL SECTION
============================== -->

<section id="TESTIMONIAL" class="testimonial parallax">
    <div class="section_overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-12 wow bounceInDown">
                    <div id="carousel-example-caption-testimonial" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-caption-testimonial" data-slide-to="0"
                                class="active"></li>
                            <li data-target="#carousel-example-caption-testimonial" data-slide-to="1"></li>
                            <li data-target="#carousel-example-caption-testimonial" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <!-- IMAGE -->
                                            <img src="images/client_1.png" alt="">

                                            <div class="testimonial_caption">
                                                <!-- DESCRIPTION -->
                                                <h2>iDEA</h2>
                                                <h4><span>Startup Bangladesh,</span> ICT Ministry</h4>

                                                <p>“Best lecture sheet, All questions in a single question Bank and
                                                    awesome Model Test. Just love it.”</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <!-- IMAGE -->
                                            <img src="images/client_2.png" alt="">

                                            <div class="testimonial_caption">
                                                <!-- DESCRIPTION -->
                                                <h2>Rahat Ibna Hossain</h2>
                                                <h4><span>CSE,</span> JUST</h4>

                                                <p>“Very informative easy and correct question bank. And i could not
                                                    think about to get chance without Model test.”</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- END TESTIMONIAL SECTION -->


<!-- =========================
     START FEATURES
============================== -->
<section id="FEATURES" class="features page">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <!-- FEATURES SECTION TITLE -->
                <div class="section_title wow fadeIn" data-wow-duration="1s">
                    <h2>Features</h2>
                    <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>-->
                </div>
                <!-- END FEATURES SECTION TITLE -->
            </div>
        </div>
    </div>
    <div class="feature_inner">
        <div class="container">
            <div class="row">
                <div class="col-md-4 right_no_padding wow fadeInLeft" data-wow-duration="1s">
                    <!-- FEATURE -->

                    <div class="left_single_feature">
                        <!-- ICON -->
                        <div><span class="pe-7s-like"></span></div>

                        <!-- FEATURE HEADING AND DESCRIPTION -->
                        <h3>মডেল টেস্ট</h3>

                        <p><span class="more">Model Test feature টি App টিকে  পছন্দের শীর্ষে নিয়ে পৌছেছে এতে রয়েছে নোটিফিকেশন option যা আপনাকে  ১ দিন আগে Model Test এর কথা স্মরণ করিয়ে দেয় ,
                            আছে সময়ের সাথে পরীক্ষা দেয়ার অপশন সেই সাথে skip অপশন যা পুরোপুরি Live Exam এর মত । শুধু তাই নয় পরীক্ষা শেষে জানতে পারবেন সারাদেশের পরীক্ষার্থীদের মধ্যো আপনার অবস্থান ও মার্ক কত এবং দেখতে পারবেন সেরা ১০০ জনের
                            তালিকা যা আপনাকে দিনে দিনে একজন Strong candidate এ পরিণত করবে </span></p>
                    </div>

                    <!-- END SINGLE FEATURE -->


                    <!-- FEATURE -->
                    <div class="left_single_feature">
                        <!-- ICON -->
                        <div><span class="pe-7s-science"></span></div>

                        <!-- FEATURE HEADING AND DESCRIPTION -->
                        <h3>সঠিক দিক নির্দেশনা</h3>

                        <p><span class="more">আপনি যদি উচ্চ মাধ্যমিকের ছাত্র-ছাত্রী হয়ে থাকেন তাহলে আপনাকে সর্বপ্রথম জানতে হবে বিশ্ববিদ্যালয়ের ভর্তি পরীক্ষা পদ্ধতি, যোগ্যতা এবং কোন সাবজেক্ট পড়লে কেমন ক্যারিয়ার গড়া যায় । ভাল ফলাফল করেও যদি আপনি ভাল কোন বিশ্ববিদ্যালয় বা মেডিকেল কলেজে ভর্তি হবার সুযোগ না পান তাহলে সে ফলাফল বৃথা ।
                                সঠিক দিকনির্দেশনাই পারে ভাল একটি বিশ্ববিদ্যালয়ে ভাল একটি বিষয়ে পড়ে খুব ভাল ক্যারিয়ার গড়ার সাহায্য করতে।</span>
                        </p>
                    </div>
                    <!-- END SINGLE FEATURE -->


                    <!-- FEATURE -->
                    <div class="left_single_feature">
                        <!-- ICON -->
                        <div><span class="pe-7s-look"></span></div>

                        <!-- FEATURE HEADING AND DESCRIPTION -->
                        <h3>HANGOUT</h3>

                        <p><span class="more">Hangout Feature টি ভর্তি পরীক্ষার্থীদের Community তৈরিতে আর এক ধাপ এগিয়ে নিয়ে গেছে । মনে করুন Exam শুরু হবে ১০.০০ টায় আপনি ১০ মিনিট আগে App Open করে বসে আছেন ঐ সময়টুকু আপনি চাইলে অনন্য পরীক্ষার্থীদের Hangout করতে পারেন ,
                            জানতে পারেন তাদের প্রস্তুতি ।</span></p>
                    </div>
                    <!-- END SINGLE FEATURE -->

                </div>
                <div class="col-md-4">
                    <div class="feature_iphone">
                        <!-- FEATURE PHONE IMAGE -->
                        <img class="wow bounceIn img-responsive" data-wow-duration="1s" src="images/iPhone_Home.png"
                             alt="">
                    </div>
                </div>
                <div class="col-md-4 left_no_padding wow fadeInRight" data-wow-duration="1s">

                    <!-- FEATURE -->
                    <div class="right_single_feature">
                        <!-- ICON -->
                        <div><span class="pe-7s-monitor"></span></div>

                        <!-- FEATURE HEADING AND DESCRIPTION -->
                        <h3>Question Bank</h3>

                        <p><span class="more">বিষয় ভিত্তিক অসংখ্য প্রশ্ন এবং বিগত সালের বিভিন্ন বিশ্ববিদ্যালয়ের প্রশ্ন সমাধানের মধ্যোমে প্রশ্নের ধরন সম্পর্কে জানতে পারেন । সেই সাথে অল্প সময়ে রিভিশন ও করতে পারেন এমনকি অনুশীলন শেষে সঠিক ও ভুল উত্তরের সংখ্যা ও জানতে পারেন ।</span>
                        </p>
                    </div>
                    <!-- END SINGLE FEATURE -->


                    <!-- FEATURE -->
                    <div class="right_single_feature">
                        <!-- ICON -->
                        <div><span class="pe-7s-phone"></span></div>

                        <!-- FEATURE HEADING AND DESCRIPTION -->
                        <h3>নোটিশ বোর্ড</h3>

                        <p><span="more">ভর্তি পরীক্ষা সংক্রান্ত সকল প্রকার নোটিশ , রেজাল্ট জানতে পারবেন অ্যাপটি থেকে
                            ।</span></p>
                    </div>
                    <!-- END SINGLE FEATURE -->


                    <!-- FEATURE -->
                    <div class="right_single_feature">
                        <!-- ICON -->
                        <div><span class="pe-7s-gleam"></span></div>

                        <!-- FEATURE HEADING AND DESCRIPTION -->
                        <h3>লেকচার শিট</h3>

                        <p>
                            <!-- <span class="less">টেকনিক সম্বলিত লেকচার শিট অ্যাপ টিকে অনন্য করে তুলেছে । জটিল ও বারবার ভুলে যাওয়া বিষয় গুলো সহজে মনে রাখতে  অ্যাপ টি অতুলনীয় । আমরা প্রায়ই ছোট দিনের উদ্ভিদ, বড় দিনের উদ্ভিদ গুলোর নাম ভুলে যায় , না হলে একটার সাথে একটা মিশিয়ে ফেলি...
                             </span>-->

                            <span class="more">টেকনিক সম্বলিত লেকচার শিট অ্যাপ টিকে অনন্য করে তুলেছে । জটিল ও বারবার ভুলে যাওয়া বিষয় গুলো সহজে মনে রাখতে  অ্যাপ টি অতুলনীয় । আমরা প্রায়ই ছোট দিনের উদ্ভিদ, বড় দিনের উদ্ভিদ গুলোর নাম ভুলে যায় , না হলে একটার সাথে একটা মিশিয়ে ফেলি, যেমন এক বন্ধু তার বান্ধবীকে শুভেচ্ছা জানাতে গিয়ে লেখা চিঠির মাধ্যমে
                            খুব সহজে আমরা ছোট দিনের উদ্ভিদ গুলোর নাম মনে রাখতে পারি ।<br>



                            চন্দ্রমল্লিকা
                            (চন্দ্রমল্লিকা), <br> কসম(কসমস) জানায়(জিনিয়া) রুপালি(রূপা আমন) কালিতে(কালোজিরা) তারার আলোয়(আলু) ডালি(ডালিয়া) সাজিয়ে(সজিনা) তোমাকে (তামাক) শুভেচ্ছা পাঠালাম(পাট) এই ছোট্ট চিঠিতে।<br>
                            ইতি(ইক্ষু) <br>
                            শিমুল(শিমুল)

                        </span>
                        </p>


                    </div>
                    <!-- END SINGLE FEATURE -->
                </div>
            </div>
        </div>
    </div>

</section>
<!-- END FEATURES SECTION -->


<!-- =========================
     START CALL TO ACTION
============================== -->
<div class="call_to_action">
    <div class="container">
        <div class="row wow fadeInLeftBig" data-wow-duration="1s">
            <div class="col-md-9">
                <p>৫০,০০০ - ৫ লক্ষ ছাত্র-ছাত্রীর সাথে মডেল টেস্ট দিয়ে আপনার প্রস্তুতি কেমন জানতে ...।</p>
            </div>
            <div class="col-md-3">
                <!-- <a class="btn btn-primary btn-action" href="<?php echo $this->Html->url("/app/files/APK/AdmissionNinja.apk", true) ?>" role="button">Download Now</a> -->
                

                   <a class="btn btn-primary btn-action"
                   href="https://play.google.com/store/apps/details?id=com.xor.admissiontest" role="button"
                   target="_blank">Download Now</a>
            </div>
        </div>
    </div>
</div>

<!-- END CALL TO ACTION -->


<!-- =========================
     Start APPS SCREEN SECTION
============================== -->
<section class="apps_screen page" id="SCREENS">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 wow fadeInBig" data-wow-duration="1s">
                <!-- APPS SCREEN TITLE -->
                <div class="section_title">
                    <h2>Screenshots</h2>
                    <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>-->
                </div>
                <!-- END APPS SCREEN TITLE -->
            </div>
        </div>
    </div>

    <div class="screen_slider">
        <div id="demo" class="wow bounceInRight" data-wow-duration="1s">
            <div id="owl-demo" class="owl-carousel">

                <!-- APPS SCREEN IMAGES -->
                <div class="item">
                    <a href="images/iphone04.png" rel="prettyPhoto[pp_gal]"><img src="images/iphone04.png" width="60"
                                                                                 height="60" alt="APPS SCREEN"/></a>
                </div>
                <div class="item">
                    <a href="images/iphone05.png" rel="prettyPhoto[pp_gal]"><img src="images/iphone05.png" width="60"
                                                                                 height="60" alt="APPS SCREEN"/></a>
                </div>
                <div class="item">
                    <a href="images/iphone06.png" rel="prettyPhoto[pp_gal]"><img src="images/iphone06.png" width="60"
                                                                                 height="60" alt="APPS SCREEN"/></a>
                </div>
                <div class="item">
                    <a href="images/iphone07.png" rel="prettyPhoto[pp_gal]"><img src="images/iphone07.png" width="60"
                                                                                 height="60" alt="APPS SCREEN"/></a>
                </div>
                <div class="item">
                    <a href="images/iphone08.png" rel="prettyPhoto[pp_gal]"><img src="images/iphone08.png" width="60"
                                                                                 height="60" alt="APPS SCREEN"/></a>
                </div>
                <div class="item">
                    <a href="images/iphone09.png" rel="prettyPhoto[pp_gal]"><img src="images/iphone09.png" width="60"
                                                                                 height="60" alt="APPS SCREEN"/></a>
                </div>
                <div class="item">
                    <a href="images/iphone10.png" rel="prettyPhoto[pp_gal]"><img src="images/iphone10.png" width="60"
                                                                                 height="60" alt="APPS SCREEN"/></a>
                </div>
                <div class="item">
                    <a href="images/iphone11.png" rel="prettyPhoto[pp_gal]"><img src="images/iphone11.png" width="60"
                                                                                 height="60" alt="APPS SCREEN"/></a>
                </div>
                <div class="item">
                    <a href="images/iphone12.png" rel="prettyPhoto[pp_gal]"><img src="images/iphone12.png" width="60"
                                                                                 height="60" alt="APPS SCREEN"/></a>
                </div>
                <div class="item">
                    <a href="images/iphone13.png" rel="prettyPhoto[pp_gal]"><img src="images/iphone13.png" width="60"
                                                                                 height="60" alt="APPS SCREEN"/></a>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ENS APPS SCREEN -->


<!-- =========================
     Start FUN FACTS
============================== -->


<section class="fun_facts parallax">
    <div class="section_overlay">
        <div class="container wow bounceInLeft" data-wow-duration="1s">
            <div class="row text-center">
                <div class="col-md-3">
                    <div class="single_fun_facts">
                        <i class="pe-7s-cloud-download"></i>

                        <h2><span class="counter_num">699</span> <span>+</span></h2>

                        <p>Downloads</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single_fun_facts">
                        <i class="pe-7s-look"></i>

                        <h2><span class="counter_num">1999</span> <span>+</span></h2>

                        <p>Likes</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single_fun_facts">
                        <i class="pe-7s-comment"></i>

                        <h2><span class="counter_num">199</span> <span>+</span></h2>

                        <p>Feedbacks</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single_fun_facts">
                        <i class="pe-7s-cup"></i>

                        <h2><span class="counter_num">10</span> <span>+</span></h2>

                        <p>Awards</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- END FUN FACTS -->


<!-- =========================
     START DOWNLOAD NOW
============================== -->
<section class="download page" id="DOWNLOAD">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <!-- DOWNLOAD NOW SECTION TITLE -->
                <div class="section_title">
                    <h2>download now</h2>

                    <p>এখন লেখাপড়া হোক সর্বত্র এবং সবসময়। আড্ডা কিংবা যাত্রাপথে যেকোনো স্থান থেকে প্রস্ত্তুিত নিতে এখনি
                        ডাউনলোড এবং ইন্সটল করুন।</p>
                </div>
                <!--END DOWNLOAD NOW SECTION TITLE -->
            </div>
        </div>
    </div>

    <!-- <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="download_screen text-center wow fadeInUp" data-wow-duration="1s">
                    <img src="images/download_screen.png" alt="">
                </div>
            </div>
        </div>
    </div> -->

    <div class="available_store">
        <div class="container  wow bounceInRight" data-wow-duration="1s">
            <br>

            <div class="col-md-12 js-alert-box" style="display: none">
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <strong>Info!</strong> Coming very soon, meanwhile please have a experience with Android. Thanks.
                </div>
            </div>
            <div class="col-md-6">
                <div class="available_title">
                    <h2>Available on</h2>

                    <p>Currently we have Android. iOS and Windows are coming soon. </p>
                </div>
            </div>

            <!-- DOWNLOADABLE STORE -->
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 no_padding js-app-download">
                        <a href="javascript:void(0)">
                            <div class="single_store">
                                <i class="fa fa-apple"></i>

                                <div class="store_inner">
                                    <h2>iOS</h2>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-4 no_padding js-android-download">
                        
                        <a href="https://play.google.com/store/apps/details?id=com.xor.admissiontest">
                            <div class="single_store">
                                <i class="fa fa-android"></i>

                                <div class="store_inner">
                                    <h2>ANDROID</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 no_padding js-app-download">
                        <a href="javascript:void(0)">
                            <div class="single_store last">
                                <i class="fa fa-windows"></i>

                                <div class="store_inner">
                                    <h2>WINDOWS</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- END DOWNLOADABLE STORE -->
        </div>
    </div>
</section>
<!-- END DOWNLOAD -->

<!-- =========================
     START CONTCT FORM AREA
============================== -->
<section class="contact page" id="CONTACT">
    <div class="section_overlay">
        <div class="container">
            <div class="col-md-10 col-md-offset-1 wow bounceIn">
                <!-- Start Contact Section Title-->
                <div class="section_title">
                    <h2>Get in touch</h2>

                    <p>We are just started and there may have issues in App. Please fell free to ask anything and give
                        us feedback to get fixed.</p>
                </div>
            </div>
        </div>

        <div class="contact_form wow bounceIn">
            <div class="container">

                <!-- START ERROR AND SUCCESS MESSAGE -->
                <div class="form_error text-center">
                    <div class="net_error hide error">Something went wrong! Please try again. Thanks.</div>
                    <div class="name_error hide error">Please Enter your name</div>
                    <div class="email_error hide error">Please Enter your Email</div>
                    <div class="email_val_error hide error">Please Enter a Valid Email Address</div>
                    <div class="message_error hide error">Please Enter Your Message</div>
                </div>
                <div class="Sucess"></div>
                <!-- END ERROR AND SUCCESS MESSAGE -->

                <!-- CONTACT FORM starts here, Go to contact.php and add your email ID, thats it.-->
                <form role="form" class="js-contact-form">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="name" placeholder="Name"
                                   name="data[Email][fullname]">
                            <input type="email" class="form-control" id="email" placeholder="Email"
                                   name="data[Email][email]">
                            <input type="text" class="form-control" id="subject" placeholder="Subject"
                                   name="data[Email][subject]">
                        </div>


                        <div class="col-md-8">
                            <textarea class="form-control" id="message" rows="25" cols="10"
                                      placeholder="  Message Texts..." name="data[Email][message]"></textarea>
                            <button type="submit" class="btn btn-default submit-btn form_submit js-contact-btn">SEND
                                MESSAGE
                            </button>
                        </div>
                    </div>
                </form>
                <!-- END FORM -->
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12 wow bounceInLeft">
                    <div class="social_icons">
                        <ul>
                            <li><a href="https://www.facebook.com/admission.ninja" target="_blank"><i
                                        class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href=""><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href=""><i class="fa fa-dribbble"></i></a>
                            </li>
                            <li><a href=""><i class="fa fa-behance"></i></a>
                            </li>
                            <li><a href=""><i class="fa fa-youtube-play"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END CONTACT -->

<!-- =========================
     Start Subscription Form
============================== -->


<section class="subscribe parallax subscribe-parallax" data-stellar-background-ratio="0.6"
         data-stellar-vertical-offset="20" id="reg">
    <div class="section_overlay wow lightSpeedIn">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <!-- Start Subscribe Section Title -->
                    <div class="section_title">
                        <h2>SUBSCRIBE US</h2>

                        <p>Admission Ninja is all in one. গ্রুপভিত্তিক লেকচার সিট, প্রশ্ন ব্যাংক এবং মডেল টেস্ট পেতে
                            নিবন্ধন করুন।</p>
                    </div>
                    <!-- End Subscribe Section Title -->
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row  wow lightSpeedIn">
                <div class="col-md-6 col-md-offset-3">
                    <!-- SUBSCRIPTION SUCCESSFUL OR ERROR MESSAGES -->
                    <div class="alert alert-success js-alert" style="width: 90%; display: none;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                        <strong>Thank You!</strong> Your registration has been successful.
                    </div>
                    <div class="subscription-error"></div>
                    <!-- Check this topic on how to add email subscription list, http://kb.mailchimp.com/lists/signup-forms/host-your-own-signup-forms -->

                    <form data-toggle="validator" role="form" id="registration_form">
                        <!--user name part-->
                        <div class="form-group has-feedback">
                            <div class="input-group ">
                                <div class="input-group-addon user_icon"><i class="fa fa-user"></i></div>
                                <input type="text" name="data[User][username]"
                                       data-remote="<?php echo $this->Html->url('/users/validate_input/false', true) ?>"
                                       id="username" class="form-control user_input"
                                       placeholder="User Name (i.e : raz.ninja5)" required
                                       data-error="This Username already exist or empty.">
                            </div>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>

                            <div class="help-block with-errors"></div>
                        </div>
                        <!-- fullname part-->
                        <div class="form-group has-feedback">
                            <div class="input-group ">
                                <div class="input-group-addon user_icon"><i class="fa fa-user"></i></div>
                                <input type="text" id="fullname" class="form-control user_input" placeholder="Full Name"
                                       name="data[User][fullname]" data-minlength="3">
                            </div>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>

                        <!--email part-->
                        <div class="form-group has-feedback">
                            <div class="input-group">
                                <div class="input-group-addon user_icon"><i class="fa fa-envelope"></i></div>
                                <input type="email" class="form-control user_input" id="inputmail1" placeholder="Email"
                                       name="data[User][email]">
                            </div>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <!--password part-->
                        <div class="form-group has-feedback">
                            <div class="input-group ">
                                <div class="input-group-addon user_icon"><i class="fa fa-lock"></i></div>
                                <input type="password" class="form-control user_input" placeholder="Password"
                                       name="data[User][password]" data-minlength="1" required>
                            </div>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <!--college name part-->
                        <div class="form-group has-feedback">
                            <div class="input-group ">
                                <div class="input-group-addon user_icon"><i class="fa fa-institution"></i></div>
                                <input type="text" class="form-control user_input" placeholder="College Name"
                                       name="data[User][college]">
                            </div>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <!--slect group name part-->
                        <div class="form-group has-feedback">
                            <div class="input-group">
                                <div class="input-group-addon user_icon"><i class="fa fa-group"></i></div>
                                <select class="form-control" name="data[User][group]" required aria-required="true">
                                    <option value="">Select your group</option>
                                    <option value="Science">Science</option>
                                    <option value="Business">Business</option>
                                    <option value="Arts">Arts</option>
                                </select>
                            </div>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <!--slect group name part-->
                        <div class="form-group has-feedback">
                            <div class="input-group">
                                <div class="input-group-addon user_icon"><i class="fa fa-mobile"></i></div>
                                <input type="text" class="form-control user_input" data-minlength="11"
                                       placeholder="Mobile Number" name="data[User][mobile]">
                            </div>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="user_sign btn">SIGN UP</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</section>

<!-- END SUBSCRIPBE FORM -->

<section class="loginback" id="login">
    <div class="section_overlay wow lightSpeedIn">
        <div class="section_title">
            <h2>Sign in</h2>
            </div>

        <div class="container">

            <div class="row  wow lightSpeedIn">
                <div class="col-md-6 col-md-offset-3">
                    <div class="alert alert-danger js-alert text-center" id="alert" style="width: 90%; display: none;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                        <strong>Alert!</strong> Incorrect Username or Password.
                    </div>

                    <form method="post" data-toggle="validator" role="form" id="login_form">
                        <!--user name part-->
                        <div class="form-group has-feedback">
                            <div class="input-group ">
                                <div class="input-group-addon user_icon"><i class="fa fa-user"></i></div>
                                <input type="text" name="user"
                                       class="form-control user_input"
                                       placeholder="User Name (i.e : raz.ninja5)" id="user" required
                                       >
                            </div>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>

                        </div>

                        <!--password part-->
                        <div class="form-group has-feedback">
                            <div class="input-group ">
                                <div class="input-group-addon user_icon"><i class="fa fa-lock"></i></div>
                                <input type="password" class="form-control user_input" placeholder="Password"
                                       name="password" id="password" required>
                            </div>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>

                        <div class="form-group">
                            <button type="button" id="login_btn" class="user_sign btn">SIGN IN</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</section>
