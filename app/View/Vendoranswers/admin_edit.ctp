<div class="vendoranswers form">
<?php echo $this->Form->create('Vendoranswer'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Vendoranswer'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('vendorquestion_id');
		echo $this->Form->input('answer');
		echo $this->Form->input('is_correct');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Vendoranswer.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Vendoranswer.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Vendoranswers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Vendorquestions'), array('controller' => 'vendorquestions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendorquestion'), array('controller' => 'vendorquestions', 'action' => 'add')); ?> </li>
	</ul>
</div>
