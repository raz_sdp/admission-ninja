<!---->
<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: mukul-->
<!-- * Date: 5/21/18-->
<!-- * Time: 8:27 PM-->
<!-- */ -->

<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: mukul-->
<!-- * Date: 5/21/18-->
<!-- * Time: 8:19 PM-->
<!-- */ -->




<?php if(@$this->params['pass']['0']){ ?>
    <!--2nd page-->
    <!DOCTYPE html>
    <html>
    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <!-- Left side column. contains the logo and sidebar -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <?php
            //pr($user)
            ?>
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Content Writter info
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Content Writter info</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">

                <div class="row">
                    <div class="col-md-3">

                        <!-- Profile Image -->
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <img class="profile-user-img img-responsive img-circle" src="<?php echo $this->Html->url('/img/'.$user['User']['pic']) ?>" alt="User profile picture" >

                                <h3 class="profile-username text-center">
                                    <?php

                                    echo($user['User']['fullname']);

                                    ?>
                                </h3>
                                <?php
                                $job = [
                                    'admin' => 'ADMIN',
                                    'cw'    => 'Content Writter',
                                    'cc'    => 'Director',
                                    'user'  => 'Student'
                                ];
                                $role = $user['User']['role'];
                                ?>
                                <p class="text-muted text-center"><?php echo($job[$role])?></p>

                                <ul class="list-group list-group-unbordered">
                                    <?php  $count = AppHelper::countQuestionSubAndUserWise($user['User']['id']) ?>
                                    <li class="list-group-item">
                                        <b>Total</b> <a class="pull-right"><?php echo($count) ?></a>
                                    </li>

                                    <li class="list-group-item">
                                        <b>City</b> <a class="pull-right"><?php echo($user['User']['city']) ?></a>
                                    </li>

                                    <li class="list-group-item">
                                        <b>Address</b> <a class="pull-right"><?php echo($user['User']['address']) ?></a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Mobile</b> <a class="pull-right"><?php echo($user['User']['mobile']) ?></a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Mail</b> <a class="pull-right"><?php echo($user['User']['email']) ?></a>
                                    </li>
                                    <?php
                                    $date = $user['User']['created'];
                                    $d    = explode(' ', $date)
                                    ?>


                                    <li class="list-group-item">
                                        <b>Member Since</b> <a class="pull-right"><?php echo($d[0]) ?></a>
                                    </li>

                                </ul>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->

                        <!-- About Me Box -->

                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-9">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#settings" data-toggle="tab">Subject Info</a></li>
                            </ul>
                            <div class="tab-content">


                                <div class=" active tab-pane" id="settings">
                                    <div class='table-responsive'>
                                        <table id="example2" class="table table-bordered table-hover">
                                            <tr>
                                                <th><?php echo $this->Paginator->sort('ID'); ?></th>
                                                <th><?php echo $this->Paginator->sort('University'); ?></th>

                                                <th><?php echo $this->Paginator->sort('Subject'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Total'); ?></th>
                                                <th class="actions"><?php echo __('Actions'); ?></th>

                                            </tr>
<?php //pr($subjects);die; ?>
                                            <?php foreach ($subjects as $subject) {?>

                                                <?php
                                                $user_id = @$this->params['pass']['0'];
                                                $total = AppHelper::countQuestionSubAndUserWise($user_id, $subject['Subject']['id']);
                                                if($total){
                                                    ?>
                                                    <tr>
                                                        <td><?php echo h($subject['Subject']['id']); ?>&nbsp;</td>
                                                        <?php $university = AppHelper::getUniversityName($subject['Subject']['university_id']) ?>
                                                        <td><?php echo h(@$university['University']['title']); ?>&nbsp;</td>
                                                        <td><?php echo h($subject['Subject']['title']); ?>&nbsp;</td>
                                                        <td><?php echo h($total); ?>&nbsp;</td>

                                                        <td class="actions">
                                                            <a target="_blank" href="<?php echo $this->Html->URL('/admin/questions/index?subject_id='.$subject['Subject']['id'].'&user_id='.$user_id) ?>"><button class="btn btn-primary">View</button> </a>
                                                        </td>
                                                    </tr>
                                                <?php } } ?>



                                        </table>

                                    <p class="pull-right">
                                        <?php
                                        echo $this->Paginator->counter(array(
                                            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                        ));
                                        ?>
                                    </p>
                                    <div class="clearfix"></div>
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <?php
                                        echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                                        echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                                        echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                                        ?>
                                    </ul>

                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- /.nav-tabs-custom -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->


        <!-- Control Sidebar -->
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
             immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->


    </body>
    </html>
<?php } else { ?>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Users of Content Writter</h3>
                    </div>
                    <!-- /.box-header -->



                    <!-- /.Search Box -->
                    <div class="box-body">
                        <div class="box-body">
                            <div class='table-responsive'>
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th><?php echo $this->Paginator->sort('id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('fullname'); ?></th>
                                        <th><?php echo $this->Paginator->sort('email'); ?></th>
                                        <th><?php echo $this->Paginator->sort('username'); ?></th>
                                        <th><?php echo $this->Paginator->sort('college'); ?></th>
                                        <th><?php echo $this->Paginator->sort('city'); ?></th>
                                        <th><?php echo $this->Paginator->sort('Total'); ?></th>
                                        <th><?php echo $this->Paginator->sort('status'); ?></th>
                                        <th><?php echo $this->Paginator->sort('created'); ?></th>
                                        <th class="actions"><?php echo __('Actions'); ?></th>

                                    </tr>

                                    </thead>
                                    <tbody>
                                   <?php foreach ($cws as $user): ?>
                                        <tr>
                                            <td><?php echo h($user['User']['id']); ?>&nbsp;</td>
                                            <td><?php echo h($user['User']['fullname']); ?>&nbsp;</td>
                                            <td><?php echo h($user['User']['email']); ?>&nbsp;</td>
                                            <td><?php echo h($user['User']['username']); ?>&nbsp;</td>
                                            <td><?php echo h($user['User']['college']); ?>&nbsp;</td>
                                            <td><?php echo h($user['User']['city']); ?>&nbsp;</td>
                                            <?php  $count = AppHelper::countQuestionSubAndUserWise($user['User']['id']) ?>
                                            <td><?php echo h($count); ?>&nbsp;</td>
                                            <?php
                                            $value = $user['User']['status'];
                                            $status = array(
                                                '1'=>'Active',
                                                '0'=>'Inactive',
                                                ''=>'Inactive'
                                            );
                                            if($status[$value]=='Active'){$values = 0;$submit = 'Inactive';}
                                            else {$values = 1;$submit = 'Active';}

                                            ?>
                                            <td id="js-status-text-<?php echo $user['User']['id']?>"><?php echo h($status[$value]); ?>&nbsp;</td>

                                            <td><?php echo h($user['User']['created']); ?>&nbsp;</td>
                                            <!--                                    <td>--><?php //echo h($user['User']['modified']); ?><!--&nbsp;</td>-->
                                            <td class="actions">
                                                <input value="<?php echo $values?>" type="hidden">
                                                <button type="button" class="btn btn-info js-status" id="<?php echo $user['User']['id']?>"><?php echo $submit?></button>
                                                <?php echo $this->Html->link(__('Details'), array('action' => 'contentwritter', $user['User']['id']), array('class' => 'btn btn-primary')); ?>
                                                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id']), array('class' => 'btn btn-warning')); ?>
                                                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $user['User']['fullname']),'class' => 'btn btn-danger')); ?>
                                            </td>
                                        </tr>

                                    <?php endforeach; ?>



                                    </tbody>

                                </table>
                            </div>


                            <p class="pull-right">
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?>
                            </p>
                            <div class="clearfix"></div>
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <?php
                                echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                                echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                                echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                                ?>
                            </ul>
                        </div>

                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<?php } ?>






