<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/23/18
 * Time: 8:58 PM
 */
?>

<div class="wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Assign Questions into '.$tests['Vendortest']['title']); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <?php echo $this->Form->create('VendortestsVendorquestion', array('type' => 'file')); ?>
                        <div class="box-body">
                            <div class="form-group"  id="test">

                                <input type="hidden" value="<?php echo $tests['Vendortest']['id'] ?>" name="data[VendortestsVendorquestion][vendortest_id]">
                            </div>

                            <div class="form-group js-question-box table-responsive" style="height: 500px;">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <th style="width:20px;"><input type="checkbox"></th>
                                    <th>Select Questions for this Test</th>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($questions1 as $key => $question) {
                                        $checked = '';
                                        $class = '';
                                        if(in_array($key, $selected_questions)) {
                                            $checked = 'checked';
                                            $class = 'success';
                                        }
                                        ?>
                                        <tr>
                                            <td><input type="checkbox" class="<?php echo $class ?>" value="<?php echo $key ?>  name="data[VendortestsVendorquestion][vendorquestion_id][]" <?php echo $checked ?>></td>
                                            <td><?php echo @$question ?></td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <button class="btn btn-primary btn-block">Save Selected Questions</button>
                            </div>
                        </div>
                        </form>
                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>
<!--<script>-->
<!--    $('#group_id').on('change', function () {-->
<!--        var group_id = $(this).val();-->
<!--        $.get(ROOT + 'admin/Vendortests/get_tests/' + group_id, function (data) {-->
<!--            $('#test').html(data);-->
<!--        });-->
<!--    });-->
<!---->
<!--    $('#group_id').on('change', function () {-->
<!--        var group_id = $(this).val();-->
<!--        $.get(ROOT + 'admin/Vendorquestions/get_question/' + group_id, function (data) {-->
<!--            $('.js-question-box').html(data);-->
<!--        });-->
<!--    });-->
<!--</script>-->































<!---->
<!---->
<!---->
<!---->
<!--<div class="vendortestsVendorquestions form">-->
<?php //echo $this->Form->create('VendortestsVendorquestion'); ?>
<!--	<fieldset>-->
<!--		<legend>--><?php //echo __('Admin Add Vendortests Vendorquestion'); ?><!--</legend>-->
<!--	--><?php
//		echo $this->Form->input('vendortest_id');
//		echo $this->Form->input('vendorquestion_id');
//	?>
<!--	</fieldset>-->
<?php //echo $this->Form->end(__('Submit')); ?>
<!--</div>-->
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->
<!---->
<!--		<li>--><?php //echo $this->Html->link(__('List Vendortests Vendorquestions'), array('action' => 'index')); ?><!--</li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Vendortests'), array('controller' => 'vendortests', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Vendortest'), array('controller' => 'vendortests', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Vendorquestions'), array('controller' => 'vendorquestions', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Vendorquestion'), array('controller' => 'vendorquestions', 'action' => 'add')); ?><!-- </li>-->
<!--	</ul>-->
<!--</div>-->
