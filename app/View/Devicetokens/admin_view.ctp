<div class="devicetokens view">
<h2><?php echo __('Devicetoken'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($devicetoken['Devicetoken']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($devicetoken['User']['id'], array('controller' => 'users', 'action' => 'view', $devicetoken['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Devicetoken'); ?></dt>
		<dd>
			<?php echo h($devicetoken['Devicetoken']['devicetoken']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Login'); ?></dt>
		<dd>
			<?php echo h($devicetoken['Devicetoken']['is_login']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Devicetoken'), array('action' => 'edit', $devicetoken['Devicetoken']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Devicetoken'), array('action' => 'delete', $devicetoken['Devicetoken']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $devicetoken['Devicetoken']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Devicetokens'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Devicetoken'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
