<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Add Answer'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <?php echo $this->Form->create('Group'); ?>
                        <div class="box-body">
                            <div class="form-group">

                                <h3><?php echo $this->Form->input('title',array('class'=>'form-control'));?></h3>

                            </div>

                            <div class="form-group">

                                <?php echo $this->Form->input('status',array('class'=>''));?>

                            </div>

                        </div>

                        <?php echo $this->Form->end(__('Submit',['class'=>'Submit'])); ?>


                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>






<!--<div class="groups form">
<?php /*echo $this->Form->create('Group'); */?>
	<fieldset>
		<legend><?php /*echo __('Admin Add Group'); */?></legend>
	<?php
/*		echo $this->Form->input('title');
		echo $this->Form->input('status');
	*/?>
	</fieldset>
<?php /*echo $this->Form->end(__('Submit')); */?>
</div>
<div class="actions">
	<h3><?php /*echo __('Actions'); */?></h3>
	<ul>

		<li><?php /*echo $this->Html->link(__('List Groups'), array('action' => 'index')); */?></li>
		<li><?php /*echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Subject'), array('controller' => 'subjects', 'action' => 'add')); */?> </li>
		<li><?php /*echo $this->Html->link(__('List Tests'), array('controller' => 'tests', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Test'), array('controller' => 'tests', 'action' => 'add')); */?> </li>
	</ul>
</div>-->
