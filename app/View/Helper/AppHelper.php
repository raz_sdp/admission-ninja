<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Helper', 'View');
App::uses('TestsQuestion', 'Model');
App::uses('Subject', 'Model');
App::uses('Question', 'Model');
App::uses('University', 'Model');
App::uses('Result', 'Model');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {
    /**
     * @param int $max_number
     * @return array
     */
    public static  function createTestTimeList($max_number = 50){
        $number_lists = [];
        for( $min = 1; $min<=$max_number; $min++) {
            $second = $min *10*60;
            $number_lists[$second] = self::secondsToWords($second);
        }
        return $number_lists;
    }

    /**
     * @param $seconds
     * @return string
     */
    public static function secondsToWords($seconds)
    {
        $ret = "";

        /*** get the days ***/
        $days = intval(intval($seconds) / (3600 * 24));
        if ($days > 0) {
            $ret .= "$days days ";
        }

        /*** get the hours ***/
        $hours = (intval($seconds) / 3600) % 24;
        if ($hours > 0) {
            $ret .= "$hours hours ";
        }

        /*** get the minutes ***/
        $minutes = (intval($seconds) / 60) % 60;
        if ($minutes > 0) {
            $ret .= "$minutes minutes ";
        }

        /*** get the seconds ***/
        $seconds = intval($seconds) % 60;
        if ($seconds > 0) {
            $ret .= "$seconds seconds";
        }
        return $ret;
    }
    public static function countSelectedQuesSubwise($test_id = null, $subject_id = null){
        $obj = new TestsQuestion();
        $options_2 = array(
            'fields'=> array(
                'COUNT(TestsQuestion.subject_id) as no_of_ques'
            ),
            'conditions' => array(
                'TestsQuestion.test_id'  => $test_id,
                'TestsQuestion.subject_id'  => $subject_id,
            )
        );
        $selected_subjects = $obj->find('first', $options_2);
        return $selected_subjects[0]['no_of_ques'];
    }
    public static function countQuesSubwise($subject_id = null){
        $obj = new Question();
        $options_2 = array(
            'fields'=> array(
                'COUNT(Question.subject_id) as no_of_ques'
            ),
            'conditions' => array(
                'Question.subject_id'  => $subject_id,
            )
        );
        $ques = $obj->find('first', $options_2);
        return $ques[0]['no_of_ques'];
    }

    /**
     * @param null $group_id
     * @return array|null
     */
    public static function getSubjectLists($group_id = null){
        $obj = new Subject();
        $query = [
            'conditions' => [
                'Subject.group_id' => $group_id,
                'Subject.board' => 0
            ]
        ];
        return $obj->find('list', $query);
    }
    public static function countQuestionSubAndUserWise($user_id=null, $subject = null){
        if(!empty($subject)){
            $obj = new Question();
            $option = [
                'conditions' => [
                    'Question.user_id' => $user_id,
                    'Question.subject_id' => $subject
                ]
            ];
            return $obj->find('count', $option);
        } else {

        $obj = new Question();
        $option = [
          'conditions' => [
              'Question.user_id' => $user_id
          ]
        ];
        return $obj->find('count', $option);
        }
    }
    public static function getUniversityName($id = null){
            $obj = new University();
        $query = [
            'recursive' => -1,
            'fields' =>[
                'University.title'
            ],
            'conditions' => [
                'University.id' => $id
            ]
        ];
        return $obj->find('first', $query);
    }
    public static function getHighstnumber($test_id = null){
        $obj = new Result();
       $data = $obj->getTop100($test_id, '1');
        return $data;
    }
    public static function participate($test_id = null){
        $obj = new Result();
        $option = [
            'conditions'=>[
                'Result.test_id' => $test_id
            ]
        ];
        return $obj->find('count', $option);
    }
}
