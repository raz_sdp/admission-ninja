<?php
$default_no_of_ans = 4;
#pr($this->data);die;
?>


<div class="wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Edit Question'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <?php echo $this->Form->create('Vendorquestion'); ?>
                            <?php echo $this->Form->input('id');?>
                            <div class="form-group">
                                <?php //echo $this->Form->input('question', ['id' => 'editor', 'escape' => false]);?>
                                <label>Question</label>
                                <textarea cols="80" id="editor" name="data[Vendorquestion][question]" rows="10"><?php echo $this->data['Vendorquestion']['question']?></textarea>
                            </div>
                            <!--<div class="form-group">
                                <label for="inputEmail3">No of answers</label>
                                <?php /*echo $this->Form->input('no_of_ans', array('label' => false, 'class' => 'form-control', 'options' => $no_of_questions, 'value' => $default_no_of_ans)); */?>
                            </div>-->


                            <div class="form-group">
                                <!--                                <textarea id="option-1" name="data[''][''][]"></textarea>-->
                                <div class="js-ans-input-box">
                                    <?php
                                    $answers = $this->data['Vendoranswer'];
                                    //pr($answers);die;
                                    foreach ($answers as $key => $answer) {
                                        $arr_key = ++$key;
                                        ?>
                                        <div class="form-group">
                                            <label>Choice <?php echo $arr_key ?></label><br>

                                            <div class="col-md-10 row">
                                                <!--                                                --><?php //echo $this->Form->input('Answer.answer.' . $arr_key, array('label' => false, 'type'=> 'textarea','id' => 'option-'.$arr_key, 'escape' => false)); ?>
                                                <textarea id="option-<?php echo $arr_key; ?>" name="data[Vendoranswer][answer][]"
                                                          cols="80" rows="10"><?php echo $answer['answer'];?></textarea>
                                            </div>
                                            <div class="col-md-2">
                                                <!--<input type="checkbox" name="data[Answer][is_correct][<?php /*echo $arr_key */ ?>]" class=""
                                                       id="AnswerIsCorrect">
                                                Accept as Answer-->
                                                <div class="form-group">
                                                    <label>
                                                        <?php
                                                        $checked = "";
                                                        if (!empty($answer['is_correct'])) {
                                                            $checked = "checked";
                                                        }
                                                        ?>
                                                        <input type="radio" value="<?php echo $arr_key ?>" id="AnswerIsCorrect"
                                                               name="data[Vendoranswer][is_correct]"
                                                               class="flat-red" <?php echo $checked ?>>
                                                        Accept as Answer
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                        <!--<div class="form-group">
                    <label for="inputEmail3"
                           class="col-sm-2">Feedback <?php /*echo $arr_key */?></label>
                    <div class="col-sm-10">
                        <?php /*echo $this->Form->input('Answer.feedback.' . $arr_key, array('label' => false, 'class' => 'form-control','escape' => false, 'value' => $answer['Answer']['feedback'])); */?>
                    </div>
                </div>-->
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Answer Description</label>
                                <textarea cols="80" id="editor1" name="data[Vendorquestion][answer_description]" rows="10"><?php echo $this->data['Vendorquestion']['answer_description']?></textarea>
                            </div>
                            <!-- --><?php
                            /*                            echo $this->Form->input('Test.test', ['class' => 'form-control', 'multiple' => 'multiple']);
                                                        */?>
                            <br>
                            <div class="col-md-12">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-block btn-primary btn-lg">Update</button>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>


<script>
    /*no of question change*/
    $(function () {
        $('#QuestionNoOfAns').on('change', function () {
            var no_of_ans = $(this).val();
            var question_id = $('#test_id').val();
            var data = {
                no_of_ans: no_of_ans,
                question_id: question_id
            };
            $.post(ROOT + 'admin/questions/get_choice_lists', data, function (data) {
                $('.js-ans-input-box').html(data);
            });
        });
    });
    CKEDITOR.replace('editor');
    CKEDITOR.replace('editor1');
    CKEDITOR.replace('option-1');
    CKEDITOR.replace('option-2');
    CKEDITOR.replace('option-3');
    CKEDITOR.replace('option-4');
    CKEDITOR.replace('option-5');
</script>




























<!--<div class="vendorquestions form">-->
<?php //echo $this->Form->create('Vendorquestion'); ?>
<!--	<fieldset>-->
<!--		<legend>--><?php //echo __('Admin Edit Vendorquestion'); ?><!--</legend>-->
<!--	--><?php
//		echo $this->Form->input('id');
//		echo $this->Form->input('user_id');
//		echo $this->Form->input('question');
//		echo $this->Form->input('answers_describtion');
//		echo $this->Form->input('Vendortest');
//	?>
<!--	</fieldset>-->
<?php //echo $this->Form->end(__('Submit')); ?>
<!--</div>-->
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->
<!---->
<!--		<li>--><?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Vendorquestion.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Vendorquestion.id')))); ?><!--</li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Vendorquestions'), array('action' => 'index')); ?><!--</li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Vendortests'), array('controller' => 'vendortests', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Vendortest'), array('controller' => 'vendortests', 'action' => 'add')); ?><!-- </li>-->
<!--	</ul>-->
<!--</div>-->
