<div class="vendorquestions view">
<h2><?php echo __('Vendorquestion'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($vendorquestion['Vendorquestion']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vendorquestion['User']['id'], array('controller' => 'users', 'action' => 'view', $vendorquestion['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo h($vendorquestion['Vendorquestion']['question']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Answers Describtion'); ?></dt>
		<dd>
			<?php echo h($vendorquestion['Vendorquestion']['answers_describtion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($vendorquestion['Vendorquestion']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($vendorquestion['Vendorquestion']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Vendorquestion'), array('action' => 'edit', $vendorquestion['Vendorquestion']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Vendorquestion'), array('action' => 'delete', $vendorquestion['Vendorquestion']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $vendorquestion['Vendorquestion']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendorquestions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendorquestion'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendortests'), array('controller' => 'vendortests', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendortest'), array('controller' => 'vendortests', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Vendortests'); ?></h3>
	<?php if (!empty($vendorquestion['Vendortest'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Group Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Instructions'); ?></th>
		<th><?php echo __('Test Time'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Start Date'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($vendorquestion['Vendortest'] as $vendortest): ?>
		<tr>
			<td><?php echo $vendortest['id']; ?></td>
			<td><?php echo $vendortest['group_id']; ?></td>
			<td><?php echo $vendortest['title']; ?></td>
			<td><?php echo $vendortest['instructions']; ?></td>
			<td><?php echo $vendortest['test_time']; ?></td>
			<td><?php echo $vendortest['status']; ?></td>
			<td><?php echo $vendortest['start_date']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'vendortests', 'action' => 'view', $vendortest['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'vendortests', 'action' => 'edit', $vendortest['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'vendortests', 'action' => 'delete', $vendortest['id']), array('confirm' => __('Are you sure you want to delete # %s?', $vendortest['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Vendortest'), array('controller' => 'vendortests', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
