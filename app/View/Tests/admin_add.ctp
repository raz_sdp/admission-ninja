<div class="wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Add Lecture'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <?php echo $this->Form->create('Test'); ?>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php echo $this->Form->input('group_id', array('class' => 'form-control js-group')); ?>
                                    </div>

                                    <div class="form-group">
                                        <?php echo $this->Form->input('title', array('class' => 'form-control')); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo $this->Form->input('test_time', ['class' => 'form-control', 'options' => AppHelper::createTestTimeList()]); ?>
                                    </div>

                                    <div class="form-group">
                                        <?php echo $this->Form->input('status', ['class' => 'form-control', 'options' => ['None' => 'None', 'Upcoming' => 'Upcoming', 'Running ' => 'Running', 'Expired' => 'Expired', 'Done' => 'Done']]); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Exam Datetime</label>
                                        <div class='input-group date' id='start_date'>
                                            <input type='text' class="form-control" name="data[Test][start_date]"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <button class="btn btn-primary btn-block">Save</button>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Instructions</label>
                                        <textarea cols="80" id="instruction" name="data[Test][instructions]"
                                                  rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>
<script>
    /*$('.js-group').on('change', function () {
        var group_id = $(this).val();
        $.get(ROOT + 'admin/subjects/get_subject_lists/'+group_id, function (data) {
            $('.js-subject-box').html(data);
        });
    });*/
    CKEDITOR.replace('instruction');
</script>