<div class="tests view">
<h2><?php echo __('Test'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($test['Test']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Subject'); ?></dt>
		<dd>
			<?php echo $this->Html->link($test['Subject']['title'], array('controller' => 'subjects', 'action' => 'view', $test['Subject']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Group'); ?></dt>
		<dd>
			<?php echo $this->Html->link($test['Group']['title'], array('controller' => 'groups', 'action' => 'view', $test['Group']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($test['Test']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Instructions'); ?></dt>
		<dd>
			<?php echo h($test['Test']['instructions']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Test Time'); ?></dt>
		<dd>
			<?php echo h($test['Test']['test_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($test['Test']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start Date'); ?></dt>
		<dd>
			<?php echo h($test['Test']['start_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($test['Test']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($test['Test']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Test'), array('action' => 'edit', $test['Test']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Test'), array('action' => 'delete', $test['Test']['id']), array(), __('Are you sure you want to delete # %s?', $test['Test']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tests'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Test'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Subject'), array('controller' => 'subjects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users Answers'), array('controller' => 'users_answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Users Answer'), array('controller' => 'users_answers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Users Answers'); ?></h3>
	<?php if (!empty($test['UsersAnswer'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Test Id'); ?></th>
		<th><?php echo __('Question Id'); ?></th>
		<th><?php echo __('Answer Id'); ?></th>
		<th><?php echo __('Test Type'); ?></th>
		<th><?php echo __('Ans Index'); ?></th>
		<th><?php echo __('Is Flag'); ?></th>
		<th><?php echo __('Ques Offset'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($test['UsersAnswer'] as $usersAnswer): ?>
		<tr>
			<td><?php echo $usersAnswer['id']; ?></td>
			<td><?php echo $usersAnswer['user_id']; ?></td>
			<td><?php echo $usersAnswer['test_id']; ?></td>
			<td><?php echo $usersAnswer['question_id']; ?></td>
			<td><?php echo $usersAnswer['answer_id']; ?></td>
			<td><?php echo $usersAnswer['test_type']; ?></td>
			<td><?php echo $usersAnswer['ans_index']; ?></td>
			<td><?php echo $usersAnswer['is_flag']; ?></td>
			<td><?php echo $usersAnswer['ques_offset']; ?></td>
			<td><?php echo $usersAnswer['created']; ?></td>
			<td><?php echo $usersAnswer['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'users_answers', 'action' => 'view', $usersAnswer['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'users_answers', 'action' => 'edit', $usersAnswer['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'users_answers', 'action' => 'delete', $usersAnswer['id']), array(), __('Are you sure you want to delete # %s?', $usersAnswer['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Users Answer'), array('controller' => 'users_answers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Questions'); ?></h3>
	<?php if (!empty($test['Question'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Subject Id'); ?></th>
		<th><?php echo __('Time'); ?></th>
		<th><?php echo __('Question'); ?></th>
		<th><?php echo __('Point'); ?></th>
		<th><?php echo __('Solution'); ?></th>
		<th><?php echo __('Answer Description'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($test['Question'] as $question): ?>
		<tr>
			<td><?php echo $question['id']; ?></td>
			<td><?php echo $question['subject_id']; ?></td>
			<td><?php echo $question['time']; ?></td>
			<td><?php echo $question['question']; ?></td>
			<td><?php echo $question['point']; ?></td>
			<td><?php echo $question['solution']; ?></td>
			<td><?php echo $question['answer_description']; ?></td>
			<td><?php echo $question['created']; ?></td>
			<td><?php echo $question['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'questions', 'action' => 'view', $question['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'questions', 'action' => 'edit', $question['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'questions', 'action' => 'delete', $question['id']), array(), __('Are you sure you want to delete # %s?', $question['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
