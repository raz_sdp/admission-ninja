



<div class="content-wrapper"">
    <section class="content">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Lecture List Of <?php echo($subject['Subject']['title']) ?></h3>
                    </div>

        <table id="example2" class="table table-bordered table-hover">
            <tr>
                <th>Title</th>
                <th>Action</th>
            </tr>

            <?php
if(!empty($lec_data)){
            foreach ($lec_data as $data):
           // print_r($data);
            ?>
            <tr>

                <td>
                    <?php echo($data['Lecture']['title'])?>
                </td>
                <td><button class="btn btn-primary">View</button></td>
            </tr>
            <?php endforeach; ?>
                <?php } else {?>
                <tr>

                    <td>
                        No Result Found
                    </td>

                </tr>
            <?php } ?>

        </table>











                </div>
                <p class="pull-right">
                    <?php
                    echo $this->Paginator->counter(array(
                        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                    ));
                    ?>
                </p>

                <div class="clearfix"></div>
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php
                    echo '<li>' . $this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')) . '</li>';
                    echo '<li>' . $this->Paginator->numbers(array('separator' => '')) . '</li>';
                    echo '<li>' . $this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')) . '</li>';
                    ?>
                </ul>
            </div>
        </div>
        </section>

</div>
