<?php //pr($results); ?>

<div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Results for <?php echo $results['0']['Test']['title'] ?></h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- /.Search Box -->
                    <div class="box-body">
                        <div class="row">
                            <?php echo $this->Form->create('Result', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>

                            <div class="col-md-12">
                                <div class="input-group input-group-sm">
                                    <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Search by Test name"
                                           value="<?php echo @$keyword ?>">
                                        <span class="input-group-btn">
                                          <button type="submit" class="btn btn-info btn-flat">Go!</button>
                                        </span>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            </form>
                            <!-- /.col -->
                        </div>

                        <!-- /.Search Box -->





                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                <tr>
                                    <th><?php echo $this->Paginator->sort('SL.'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Username'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Full name'); ?></th>
                                    <th><?php echo $this->Paginator->sort('College'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Mark'); ?></th>
                                    <th class="actions"><?php echo __('Actions'); ?></th>
                                </tr>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $cnt = '1';
                                foreach ($results as $result):
                                    ?>
                                    <tr>
                                        <td><?php echo h($cnt++); ?>&nbsp;</td>
                                        <td>
                                            <?php echo h($result['User']['username']); ?>
                                        </td>
                                        <td>
                                            <?php echo h($result['User']['fullname']); ?>
                                        </td>
                                        <td><?php echo h($result['User']['college']); ?>&nbsp;</td>
                                        <td><?php echo h($result['Result']['mark']); ?>&nbsp;</td>
                                        <td class="actions">
                                            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $result['Result']['id']), array('confirm' => __('Are you sure you want to delete  %s result?', $result['User']['fullname']), 'class' => 'btn btn-danger')); ?>
                                        </td>
                                    </tr>

                                <?php endforeach; ?>

                                </tbody>

                            </table>
                            <p class="pull-right">
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?>
                            </p>
                            <div class="clearfix"></div>
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <?php
                                echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                                echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                                echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                                ?>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
