<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/23/18
 * Time: 8:58 PM
 */
?>

<div class="wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Assign Questions into ' . $tests['Test']['title']); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <?php echo $this->Form->create('TestsQuestion', array('type' => 'file'));

                        ?>
                        <div class="box-body">
                            <div class="form-group">
                                <?php echo $this->Form->input('group_id', ['class' => 'form-control js-group', 'value' => $group_id]); ?>
                            </div>
                            <div class="form-group">
                                <?php
                                /*$options = array('M' => 'Male', 'F' => 'Female');
                                $attributes = array('legend' => false);
                                echo $this->Form->radio('board', $options, $attributes);*/
                                ?>
                                <div class="col-md-4">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="check" class="SubjectBoard" value="0"
                                                   aria-label="...">
                                            বিষয়ভিত্তিক
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="check" class="SubjectBoard" value="1"
                                                   aria-label="..." checked>
                                            বিশ্ববিদ্যালয়ের বিগত বছরের প্রশ্ন ?
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="check" class="SubjectBoard" value="2"
                                                   aria-label="...">
                                            মডেল টেস্ট
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <center><img src="<?php echo $this->Html->url('/img/ajax.gif') ?>" class="img"
                                             style="display: none"></center>
                            </div>

                            <div class="form-group js-subject-div">

                                <?php //echo $this->Form->input('subject_id', array('class' => 'form-control select2box js-subject')); ?>
                                <select class="form-control js-subject" name="data[TestsQuestion][subject_id]">
                                    <?php
                                    foreach ($subjects as $key => $subject) {
                                        $count_sel_ques = AppHelper::countSelectedQuesSubwise($test_id, $key);
                                        ?>
                                        <option value="<?php echo $key ?>"
                                                class="<?php echo !empty($count_sel_ques) ? 'btn-success' : '' ?>"><?php echo $subject . ' (' . $count_sel_ques . ' questions taken from this subject)' ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group js-question-box table-responsive" style="height: 500px;">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <th style="width:20px;"><input type="checkbox"></th>
                                    <th>Select Questions for this Test</th>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($questions as $key => $question) {
                                        $checked = '';
                                        $class = '';
                                        if (in_array($key, $selected_questions)) {
                                            $checked = 'checked';
                                            $class = 'success';
                                        }
                                        ?>
                                        <tr class="<?php echo $class ?>">
                                            <td><input type="checkbox" value="<?php echo $key ?>"
                                                       name="data[TestsQuestion][question_id][]" <?php echo $checked ?>>
                                            </td>
                                            <td><?php echo $question ?></td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <button class="btn btn-primary btn-block">Save Selected Questions</button>
                            </div>
                        </div>
                        </form>
                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>
<script>
    var test_id = <?php echo $test_id?>;
    $('.js-subject').on('change', function () {
        var subject_id = $(this).val();
        $.get(ROOT + 'admin/questions/get_questions_subject_wise/' + subject_id + '/' + test_id, function (data) {
            $('.js-question-box').html(data);
        });
    });
    $('.SubjectBoard').on('change', function () {
        $('.img').show();
        $('.js-subject-div').hide();
        var query = $(this).val();
        var group_id = $('.js-group').val();
        commonSearch(query, group_id);
    });
    $('.js-group').on('change', function () {
        $('.img').show();
        $('.js-subject-div').hide();
        var query = $('.SubjectBoard:checked').val();
        var group_id = $(this).val();
        commonSearch(query, group_id);
    });
    function commonSearch(query, group) {

        $.get(ROOT + 'admin/subjects/searchsub/' + query + '/' + group + '/' + test_id, function (data) {
            $('.js-subject-div').show().html(data);

            /*Manage Questions*/
            var subject_id = $('.js-subject').val();
            $.get(ROOT + 'admin/questions/get_questions_subject_wise/' + subject_id + '/' + test_id, function (data) {
                $('.js-question-box').html(data);
            });

            $('.img').hide();

        });
    }
</script>


