                
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title text-center"><?php echo $test_title;?></h3>
    </div>
    <div class="panel-body">
        <?php
        foreach($data as $key => $item){
            ?>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo ++$key.'. '.@$item['question'];?></h3>
                    </div>
                    <div class="panel-body">
                        <ul class="list-group">
                            <li class="list-group-item <?php echo 'options1' == "options$item[answer]" ? 'list-group-item-success' : ''?>">A. <?php echo @$item['options1'];?></li>
                            <li class="list-group-item <?php echo 'options2' == "options$item[answer]" ? 'list-group-item-success' : ''?>">B. <?php echo @$item['options2'];?></li>
                            <li class="list-group-item <?php echo 'options3' == "options$item[answer]" ? 'list-group-item-success' : ''?>">C. <?php echo @$item['options3'];?></li>
                            <li class="list-group-item <?php echo 'options4' == "options$item[answer]" ? 'list-group-item-success' : ''?>">D. <?php echo @$item['options4'];?></li>
                        </ul>
                    </div>
                </div>   
            </div>
            <?php
            if($key%3==0) echo '<div class="clearfix"></div>';
        }
        ?>                     
    </div>
</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  