
<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin View Model Test'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body">
                            <div class="form-group">
                                <h3><?php echo $this->Form->input('test_id',array('class'=>'form-control js-test'));?></h3>
                            </div>

                            <div class="js-model-ques-paper-box">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title text-center"><?php echo $test_title;?> <br> Directed By Raz</h3>
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                        foreach($data as $key => $item){
                                            ?>
                                            <div class="col-md-4">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <a href="<?php echo $this->Html->url('/admin/questions/edit/'.$item['id']) ?>"><span class="glyphicon glyphicon-edit pull-right glyphicon-custom" aria-hidden="true"></span></a>
<!--                                                        <span>--><?php //echo $this->Html->link(__('<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>'), array('controller' => 'questions','action' => 'edit', $item['id']),array('class'=>'btn','escape' => false)); ?><!--</span>-->
                                                        <h3 class="panel-title"><?php echo ++$key.'. '.@$item['question'];?></h3>

                                                    </div>
                                                    <div class="panel-body">
                                                        <ul class="list-group">
                                                            <li class="list-group-item <?php echo 'options1' == "options$item[answer]" ? 'list-group-item-success' : ''?>">A. <?php echo @$item['options1'];?></li>
                                                            <li class="list-group-item <?php echo 'options2' == "options$item[answer]" ? 'list-group-item-success' : ''?>">B. <?php echo @$item['options2'];?></li>
                                                            <li class="list-group-item <?php echo 'options3' == "options$item[answer]" ? 'list-group-item-success' : ''?>">C. <?php echo @$item['options3'];?></li>
                                                            <li class="list-group-item <?php echo 'options4' == "options$item[answer]" ? 'list-group-item-success' : ''?>">D. <?php echo @$item['options4'];?></li>
                                                        </ul>
                                                    </div>
                                                </div>   
                                            </div>
                                            <?php
                                            if($key%3==0) echo '<div class="clearfix"></div>';
                                        }
                                        ?>                     
                                    </div>
                                </div>   
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>