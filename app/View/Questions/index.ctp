<div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Questions List</h3>
                    </div>
                    <?php
                    //pr($questions);die;
                    ?>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <?php echo $this->Form->create('Question', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">Subject</label>
                                        <div class="col-sm-10">
                                            <?php echo $this->Form->input('subject_id', array(
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'options' => ['' =>'- None -'] + $subjects,
                                                    'value' => $subject_id
                                                )
                                            ); ?>
                                        </div>
                                    <!-- /.form-group -->
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-3">
                                    <div class="input-group input-group-sm">
                                        <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Search by Question name"
                                               value="<?php echo $keyword ?>">
                                        <span class="input-group-btn">
                                          <button type="submit" class="btn btn-info btn-flat">Go!</button>
                                        </span>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </form>
                            <!-- /.col -->
                        </div>

                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('id'); ?></th>
                                <th><?php echo $this->Paginator->sort('user_id', 'Content Writer'); ?></th>
                                <th><?php echo $this->Paginator->sort('subject_id'); ?></th>
                                <!--			<th>--><?php //echo $this->Paginator->sort('time'); ?><!--</th>-->
                                <th><?php echo $this->Paginator->sort('question'); ?></th>
                                
                                <th><?php echo $this->Paginator->sort('answer', 'Correct Answer'); ?></th>
                                <th><?php echo $this->Paginator->sort('answer_description'); ?></th>
                                <th><?php echo $this->Paginator->sort('created'); ?></th>
                                <!--<th><?php echo $this->Paginator->sort('modified'); ?></th>-->
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($questions as $question):
                            //print_r($question);die;
                            ?>
                                <tr>
                                    <td><?php echo h($question['Question']['id']); ?>&nbsp;</td>
                                    <td>
                                        <?php echo $this->Html->link($question['User']['fullname'], array('controller' => 'users', 'action' => 'view', $question['User']['id'])); ?>
                                    </td>
                                    <td>
                                        <?php echo $this->Html->link($question['Subject']['title'], array('controller' => 'subjects', 'action' => 'view', $question['Subject']['id'])); ?>
                                    </td>
                                    <!--		<td>-->
                                    <?php //echo h($question['Question']['time']); ?><!--&nbsp;</td>-->
                                    <td><?php echo $question['Question']['question']; ?>&nbsp;</td>
                                    <td><?php echo $question['Answer'][0]['answer']; ?>&nbsp;</td>
                                    <!--		<td>-->
                                    <?php //echo h($question['Question']['point']); ?><!--&nbsp;</td>-->
                                    <!--		<td>-->
                                    <?php //echo h($question['Question']['solution']); ?><!--&nbsp;</td>-->
                                    <td><?php echo h($question['Question']['answer_description']); ?>&nbsp;</td>
                                    <td><?php echo h($question['Question']['created']); ?>&nbsp;</td>
                                    <!--<td><?php echo h($question['Question']['modified']); ?>&nbsp;</td>-->
                                    <td class="actions">
                                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $question['Question']['id'])); ?>
                                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $question['Question']['id'])); ?>
                                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $question['Question']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $question['Question']['id']))); ?>
                                    </td>
                                </tr>

                            <?php endforeach; ?>

                            </tbody>

                        </table>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                        </p>

                        <div class="clearfix"></div>
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <?php
                            echo '<li>' . $this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')) . '</li>';
                            echo '<li>' . $this->Paginator->numbers(array('separator' => '')) . '</li>';
                            echo '<li>' . $this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')) . '</li>';
                            ?>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>








