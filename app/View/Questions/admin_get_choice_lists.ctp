<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 4/27/16
 * Time: 9:42 PM
 */
?>
<?php
for($count = 1; $count<=$no_of_ans; $count++){
    ?>
    <!--<div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Choice <?php /*echo $count*/?></label>
        <div class="col-sm-7">
            <?php /*echo $this->Form->input('Answer.answer.'.$count, array('label' => false,'class' => 'form-control', 'value' => $answers[$count-1]['Answer']['answer'],'type'=> 'textarea','id' => 'option-'.$count)); */?>
        </div>
        <div class="col-sm-2">

            <input type="checkbox" name="data[Answer][is_correct][<?php /*echo $count*/?>]" class=""  value="1" id="AnswerIsCorrect" <?php /*echo $checked*/?>>
            <?php /*#echo $this->Form->input('Answer.is_correct.', array('label' => false,'type' => 'checkbox', 'class' => 'form-control','data-validation' => "required", 'data-validation-error-msg'=>" ")); */?>
            Accept as Answer
        </div>

    </div>-->

    <div class="form-group">
        <label>Choice <?php echo $count ?></label><br>

        <div class="col-md-10 row">
            <textarea id="option-<?php echo $count; ?>" name="data[Answer][answer][<?php echo $count ?>]"
                      cols="80" rows="10" value="<?php echo @$answers[$count-1]['Answer']['answer']?>"></textarea>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>
                    <?php
                    $checked = "";
                    if(!empty($answers[$count-1]['Answer']['is_correct'])) {
                        $checked = "checked";
                    }
                    ?>
                    <input type="radio" value="1" id="AnswerIsCorrect"
                           name="data[Answer][is_correct][<?php echo $count ?>]"
                           class="flat-red" <?php echo @$checked?>>
                    Accept as Answer
                </label>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
<?php
}
?>
