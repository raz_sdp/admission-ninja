<div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
    <section class="content">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Questions List</h3>
                    </div>
                    <?php
                    //pr($questions);die;
                    ?>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="<?php echo !empty($subject_id) || !empty($user_id) ? 'hide' : ''?>">
<!--                            --><?php //echo $this->Form->create('Question', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>
                            <div class="form-group" style="margin-left: 1px !important">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-sm-10">
                                            <?php echo $this->Form->input('group_id', array(
                                                    'label' => false,
                                                    'class' => 'form-control js-group',
                                                    'options' => ['' =>'Select Group'] + $groups,

                                                )
                                            ); ?>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="board" class="SubjectBoard" value="0" aria-label="..."   >
                                            বিষয়ভিত্তিক
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="board" class="SubjectBoard" value="1" aria-label="..."  checked >
                                            বিশ্ববিদ্যালয়ের বিগত বছরের প্রশ্ন ?
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="board" class="SubjectBoard" value="2" aria-label="..." >
                                            মডেল টেস্ট
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-md-5 col-sm-12">
                                    <div class="form-group">
                                        <label class="col-sm-1 col-md-2 control-label">Subject</label>
                                        <div class="col-sm-10 js-subject-div">
                                            <?php echo $this->Form->input('subject_id', array(
                                                    'label' => false,
                                                    'class' => 'form-control col-md-3 js-subject',
                                                    'options' => ['' =>'- None -'] + $subjects,
                                                )
                                            ); ?>
                                        </div>
                                    <!-- /.form-group -->
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">User</label>
                                        <div class="col-sm-10">
                                            <?php echo $this->Form->input('user_id', array(
                                                    'label' => false,
                                                    'class' => 'form-control js-user',
                                                    'options' => ['' =>'- None -'] + $users,

                                                )
                                            );
                                            ?>
                                        </div>
                                    <!-- /.form-group -->
                                    </div>
                                </div>
                                <!-- /.col -->
                            <?php echo $this->Form->create('Question', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>

                            <div class="col-md-3 col-sm-12">
                                    <div class="input-group input-group-sm">
                                        <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Search by Question name"
                                               value="<?php echo $keyword ?>">
                                        <span class="input-group-btn">
                                          <button type="submit" class="btn btn-info btn-flat">Go!</button>
                                        </span>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </form>

                            <!-- /.col -->
                        </div>
                        </div>
                        <div class="js-table-box">
                        <div class='table-responsive'>
                        <table id="example2" class="table table-bordered table-hover ">
                            <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('id'); ?></th>
                                <th><?php echo $this->Paginator->sort('user_id', 'Content Writer'); ?></th>
                                <th><?php echo $this->Paginator->sort('subject_id'); ?></th>
                                <!--			<th>--><?php //echo $this->Paginator->sort('time'); ?><!--</th>-->
                                <th><?php echo $this->Paginator->sort('question'); ?></th>
                                
                                <th><?php echo $this->Paginator->sort('answer', 'Correct Answer'); ?></th>
                                <th><?php echo $this->Paginator->sort('answer_description'); ?></th>
                                <th><?php echo $this->Paginator->sort('created'); ?></th>
                                <!--<th><?php echo $this->Paginator->sort('modified'); ?></th>-->
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($questions as $question):
                            //print_r($question);die;
                            ?>
                                <tr>
                                    <td><?php echo h($question['Question']['id']); ?>&nbsp;</td>
                                    <td>
                                        <?php echo $question['User']['fullname']; ?>
                                    </td>
                                    <td>
                                        <?php echo $this->Html->link($question['Subject']['title'], array('controller' => 'subjects', 'action' => 'view', $question['Subject']['id'])); ?>
                                    </td>
                                    <!--		<td>-->
                                    <?php //echo h($question['Question']['time']); ?><!--&nbsp;</td>-->
                                    <td><?php echo $question['Question']['question']; ?>&nbsp;</td>
                                    <td><?php echo $question['Answer'][0]['answer']; ?>&nbsp;</td>
                                    <!--		<td>-->
                                    <?php //echo h($question['Question']['point']); ?><!--&nbsp;</td>-->
                                    <!--		<td>-->
                                    <?php //echo h($question['Question']['solution']); ?><!--&nbsp;</td>-->
                                    <td><?php echo h($question['Question']['answer_description']); ?>&nbsp;</td>
                                    <td><?php echo h($question['Question']['created']); ?>&nbsp;</td>
                                    <!--<td><?php echo h($question['Question']['modified']); ?>&nbsp;</td>-->
                                    <td class="actions">
                                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $question['Question']['id']), ['class'=>'btn btn-info']); ?>
                                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $question['Question']['id']),['class'=>'btn btn-success']); ?>
                                        <?php 
                                        $role= AuthComponent::user()['role'];
                                        if($role=='admin'){
                                            echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $question['Question']['id']), array('class'=>'btn btn-warning','confirm' => __('Are you sure you want to delete # %s?', $question['Question']['id']))); 
                                        }
                                        ?>
                                    </td>
                                </tr>

                            <?php endforeach; ?>

                            </tbody>

                        </table>
                        </div>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                        </p>

                        <div class="clearfix"></div>
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <?php
                            echo '<li>' . $this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')) . '</li>';
                            echo '<li>' . $this->Paginator->numbers(array('separator' => '')) . '</li>';
                            echo '<li>' . $this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')) . '</li>';
                            ?>
                        </ul>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<script type="application/javascript">
    $('.js-subject').on('change', function () {
        var subject_id = $(this).val();
        $.get(ROOT + 'admin/questions/get_questions/' + subject_id, function (data) {
            $('.js-table-box').html(data);
        });
    });
    $('#user_id').on('change', function () {
        var user_id = $(this).val();
        var subject_id = $('.js-subject').val();
        $.get(ROOT + 'admin/questions/get_question_subject_wise/' +user_id+'/'+subject_id, function (data) {
            $('.js-table-box').html(data);
        });
    });
    $('.SubjectBoard').on('change', function () {
//        $('.img').show();
//        $('.js-subject-div').hide();
        var query = $(this).val();
        var group_id = $('.js-group').val();
        commonSearch(query, group_id);
    });
    $('.js-group').on('change', function () {
//        $('.img').show();
//        $('.js-subject-div').hide();
        var query = $('.SubjectBoard:checked').val();
        var group_id = $(this).val();
        commonSearch(query, group_id);
    });
    function commonSearch(query, group){
        $.get(ROOT + 'admin/subjects/subject/' + query+'/'+group, function (data) {
            $('.js-subject-div').html(data);
            /*Manage Questions*/
//            var subject_id = $('.js-subject').val();
//            $.get(ROOT + 'admin/questions/get_questions_subject_wise/' + subject_id+'/'+test_id, function (data) {
//                $('.js-question-box').html(data);
//            });
//
//            $('.img').hide();
        });
    }
</script>








