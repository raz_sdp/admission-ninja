<style>
    .ui-state-active h4,
    .ui-state-active h4:visited {
        color: #26004d ;
    }

    .ui-menu-item{
        height: 50px;
        border: 1px solid #ececf9;
    }
    .ui-widget-content .ui-state-active {
        background-color: white !important;
        border: none !important;
    }
    .list_item_container {
        width:600px;
        height: 50px;
        float: left;
        margin-left: 20px;
    }
    .ui-widget-content .ui-state-active .list_item_container {
        background-color: #f5f5f5;
    }

    .image {
        width: 15%;
        float: left;
        padding: 5px;
    }
    .image img{
        width: 40px;
        height : 40px;
    }
    .label{
        width: 85%;
        float:right;
        white-space: nowrap;
        overflow: hidden;
        color: black;
        text-align: left;
    }
    input:focus{
        background-color: #f5f5f5;
    }

</style>

<div class="wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Add Member in Your Group'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <?php
                        $vendor_id = '';
                        if(!empty($this->params['pass']['0'])) {
                            $vendor_id = $this->params['pass']['0'];
                        }
                        ?>
                        <!-- form start -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group hide">
                                        <?php echo $this->Form->input('vendor', array('class' => 'form-control js-group', 'value' => $id)); ?>
                                    </div>

                                    <div class="form-group">
                                        <!-- search box container starts  -->

                                        <div class="well">
                                            <div class="row">
                                                <div class="col-lg-10 col-lg-offset-1">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" style="color: white; background-color: #5b518b">Username</span>
                                                        <input type="text"  name="data[Vendor][user_id]" autocomplete="off" id="search" class="form-control input-lg" placeholder="Enter Students Username">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- search box container ends  -->
                                        <div style="/*padding-top:280px;*/" ></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 js-table-data">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?php echo $this->Paginator->sort('#'); ?></th>
                                        <th><?php echo $this->Paginator->sort('fullname', 'Full Name'); ?></th>
                                        <th><?php echo $this->Paginator->sort('Username'); ?></th>
                                        <!--			<th>--><?php //echo $this->Paginator->sort('time'); ?><!--</th>-->
                                        <th><?php echo $this->Paginator->sort('Picture'); ?></th>
                                        <th class="Action">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($members as $key=>$member){ ?>
                                        <tr>
                                            <td><?php echo ++$key ?></td>

                                            <td><?php echo $member['User']['fullname'] ?></td>
                                            <td><?php echo $member['User']['username'] ?></td>
                                            <td><?php echo $this->Html->image($member['User']['pic'] , ['height' => '40px','width' => '40px','class' => 'img-circle','alt' => 'User Image'])?> </td>
                                            <?php  $admin = @$this->params['pass']['0'] ?>
                                            <td><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $member['Vendoruser']['id'],$admin) ,array('class'=>'btn btn-danger','confirm' => __('Are you sure you want to kick  %s?', $member['User']['fullname'])));?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                <p class="pull-right">
                                    <?php
                                    echo $this->Paginator->counter(array(
                                        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                    ));
                                    ?>
                                </p>

                                <div class="clearfix"></div>
                                <ul class="pagination pagination-sm no-margin pull-right">
                                    <?php
                                    echo '<li>' . $this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')) . '</li>';
                                    echo '<li>' . $this->Paginator->numbers(array('separator' => '')) . '</li>';
                                    echo '<li>' . $this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')) . '</li>';
                                    ?>
                                </ul>

                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>
<!--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript">
    $(document).ready(function(){
        var vendor_id = <?php echo $vendor_id ?>;
        $("#search").autocomplete({
            source: ROOT+"admin/users/get_username",
            focus: function( event, ui ) {
                //console.log( ui.item.username)
                //$( "#search" ).val( ui.item.username ); // uncomment this line if you want to select value to search box
                return false;
            },
            select: function( event, ui ) {
                //window.location.href = ui.item.url;
                $( "#search" ).val( '' );
                var user_id = ui.item.id;
                     console.log(vendor_id)
                $.get(ROOT + 'admin/vendorusers/insert_member/' + user_id +'/'+vendor_id ,function (res) {
                    if(!res.success) {
                        swal('Alert',res.msg,'warning');
                    } else {
                        $('.js-table-data').html(res) ;
                    }

                },'json');
                return false;
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            var inner_html = '<div class="list_item_container"><div class="image"><img src='+ROOT+'img/' + item.pic + ' ></div><div class="label"><h4><b>' + item.username + '</b></h4></div></div></a>';
            return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append(inner_html)
                .appendTo( ul );
        };
    });
</script>









