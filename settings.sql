/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : an_test

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2018-05-28 14:44:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `settings`
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `is_free` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES ('1', '1');
INSERT INTO `settings` VALUES ('2', '0');
INSERT INTO `settings` VALUES ('3', '0');
INSERT INTO `settings` VALUES ('4', '0');
INSERT INTO `settings` VALUES ('5', '0');
INSERT INTO `settings` VALUES ('6', '0');
INSERT INTO `settings` VALUES ('7', '0');
INSERT INTO `settings` VALUES ('8', '0');
